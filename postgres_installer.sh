#!/bin/bash

 DB=mep_db

# ATTENTION: do not use any repository.
 sudo apt-get install postgresql postgresql-contrib
 apt-cache search postgres
 sudo apt-get install libpq-dev
 sudo apt-get install pgadmin3

# for ubuntu 16.04
 sudo apt-get install postgis

# for ubuntu 14.04
# sudo apt-get install -y postgis postgresql-9.3-postgis-2.1


 sudo service postgresql start

 sudo -u postgres psql postgres
 # \password postgres # to initialize the password of the superuser

 sudo -u postgres createdb mep_db

 sudo -u postgres psql mep_db

# Create the postgis extension into the mep database
 PGEXT="CREATE EXTENSION postgis;"
 psql -d $DB -c "$PGEXT"

# Create the postgis_topology extension into the mep database
 PGTOPEXT="CREATE EXTENSION postgis_topology;"
 psql -d $DB -c "$PGTOPEXT"