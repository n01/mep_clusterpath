drop table public.cluster_queue;

create table public.cluster_queue(
  id serial not null,           -- serialized id
  location VARCHAR(30),         -- location of the acquisition
  pt_num int,                   -- number of points in a city
  priority int,                 -- the priority of the item
  cluster_flag bool,            -- clustered flag
  cluster_timestamp timestamp,  -- the timestamp corresponding to the clustering
  cluster_repeat_flag bool,     -- re_cluster flag
  primary key (location)
);

create or replace function update_clusterpath_queue(loc varchar) returns void as $$
begin
  if exists(select * from public.cluster_queue where location=loc) then
      update cluster_queue set cluster_repeat_flag = true where location=loc;
  else
      insert into public.cluster_queue (location, pt_num, priority, cluster_flag, cluster_timestamp, cluster_repeat_flag)
        values(loc,  (select count(*) from public.pose_pt_gnss as gnss where gnss.location=loc), 1, FALSE, (select clock_timestamp()), TRUE);
  end if;
end;
$$ language plpgsql;

-- Further details can be found on the mep-clusterpath documentation.