﻿create table public.highway_segment
(
	id serial not null,
	highway_id bigint not null,
	osm_id character varying,
	name character varying,
	segment geometry,
	primary key(id)
);