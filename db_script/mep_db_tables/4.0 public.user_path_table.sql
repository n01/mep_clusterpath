﻿CREATE TABLE public.user_path
(
    ID SERIAL PRIMARY KEY,
    device_id VARCHAR(16), -- device_id of the user
    user_email VARCHAR(50), -- user email
    time_zone TIMESTAMP, -- timestamp
    location VARCHAR(30), -- location of the acquisition
    path geometry	-- the path of the user
);