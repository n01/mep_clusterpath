drop table public.obstacle;

create table public.obstacle
(
    user_name TEXT NOT NULL,
    time_stamp BIGINT NOT NULL,
    location TEXT,
    latitude DOUBLE PRECISION,
    longitude DOUBLE PRECISION,
    point GEOMETRY,
    narrow_path BOOLEAN DEFAULT FALSE,
    flooring BOOLEAN DEFAULT FALSE,
    blocked_path BOOLEAN DEFAULT FALSE,
    steps BOOLEAN DEFAULT FALSE,
    inclined_path BOOLEAN DEFAULT FALSE,
    crosswalk_middle BOOLEAN DEFAULT FALSE,
    crosswalk_sidewalk BOOLEAN DEFAULT FALSE,
    other BOOLEAN DEFAULT FALSE,
    temporariness INT,
    criticality_level INT,
    -- photo_1 BYTEA,
    -- photo_2 BYTEA,
    -- photo_3 BYTEA,
    description TEXT,
    PRIMARY KEY (user_name, time_stamp)
);
