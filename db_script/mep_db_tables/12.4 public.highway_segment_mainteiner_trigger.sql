﻿create trigger highway_segment_unique
after insert or update on public.highway_segment for each row
execute procedure public.highway_segment_mainteiner();
