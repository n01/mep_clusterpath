create or replace function building.update_building_segment() returns trigger as $$

begin
	with 
	sample(geom, id) as
	  (select line, id, name 
		from (select distinct tmp.ln as line, tmp.ogc_fid as id, tmp.name 
			from (select ST_ExteriorRing((ST_Dump(new.wkb_geometry)).geom) as ln, new.ogc_fid, new.name) as tmp) as sp),
				
	line_counts (cts, id) as 
	  (select ST_NPoints(geom) -1 , id from sample),
	  
	series (num , id) as 
	  (select generate_series(1, cts), id from line_counts)
	  
	insert into public.building_segment(building_id, name, segment)
	  select series.id as building_id, name, ST_MakeLine(ST_PointN(geom, num), ST_PointN(geom, num + 1)) as segment
	  from series inner join sample on series.id = sample.id;
	  --where segment <> (select segment from public.building_segment);
	return new;

end;
$$ language 'plpgsql';

drop trigger building_segment_trig_update on building.multipolygons;

create trigger building_segment_trig_update
after insert or update on building.multipolygons
for each row execute procedure building.update_building_segment();