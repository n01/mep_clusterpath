﻿create or replace function building.update_building_perimeter() returns trigger as $$

  begin

    if exists(with
                  old_perimeters(geom) as (select perimeter from public.building_perimeter),
                  new_perimeter(geom) as (select ST_ExteriorRing((ST_Dump(new.wkb_geometry)).geom))
                select * from old_perimeters as op, new_perimeter as np
                where np.geom = op.geom) then
      return null;
    else
      insert into public.building_perimeter(building_id, name, perimeter)
        select pline.ogc_fid as building_id, pline.name, ST_ExteriorRing((ST_Dump(pline.wkb_geometry)).geom) as perimeter
        from (select distinct ogc_fid, wkb_geometry, name  from building.multipolygons where wkb_geometry = new.wkb_geometry) as pline;
      return new;
    end if;
  end;
$$ language 'plpgsql';

drop trigger building_perimeter_trig_update on building.multipolygons;

create trigger building_perimeter_trig_update
after insert or update on building.multipolygons
for each row execute procedure building.update_building_perimeter();