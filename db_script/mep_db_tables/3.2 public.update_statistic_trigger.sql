﻿CREATE TRIGGER update_statistic_table
  AFTER INSERT
  ON public.user_path
  FOR EACH ROW
  EXECUTE PROCEDURE public.update_statistics();