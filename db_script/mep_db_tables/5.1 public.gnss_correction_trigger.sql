﻿create or replace function public.pose_building_correction() returns trigger as $$
-- version 0.8 - 2016-11-16 - passaggio al PL/pgSQL
-- porta i punti al di fuori degli edifici
-- punti corretti in una nuova tabella

  declare
    nrows integer;
    sql1 varchar;
    sql2 varchar;
    sql3 varchar;
    res1 record;
    punto_su_linea geometry;

  begin
    -- dpthg => distanza asse stradale - punto GPS
    sql1 := 'select ST_Distance(ST_Transform(hg.seg,32632), ST_Transform(pt.coord,32632)) as dpthg, ST_Intersection(ST_MakeLine(ST_ClosestPoint(hg.seg, pt.coord),pt.coord),ln.seg) as punto_su_linea, pt.time as time
              from public.gpsdata_test as pt, public.highway_segment as hg, public.building_area as bl, public.building_segment as ln
              where ST_Contains(bl.area, pt.coord) and pt.id=$1 and ST_Distance(ST_Transform(hg.seg,32632), ST_Transform(pt.coord,32632))<100 and Not ST_IsEmpty(ST_Intersection(ST_MakeLine(ST_ClosestPoint(hg.seg, pt.coord),pt.coord),ln.segment))
              order by dpthg asc';

    sql2 := 'select device_id, ts from public.pose_pt_gnss where device_id=$1 and ts=$2';

    sql3 := 'select ST_Distance(ST_Transform(hg.segment,32632), ST_Transform(pt.coord,32632)) as dpthg, ST_Intersection(ST_MakeLine(ST_ClosestPoint(hg.segment, pt.coord),pt.coord),ln.segment) as punto_su_linea
              from public.pose_pt as pt, public.highway_segment as hg, public.building_area as bl, public.building_segment as ln
              where ST_Contains(bl.area, pt.coord) and pt.device_id=$1 and pt.ts=$2 and ST_Distance(ST_Transform(hg.segment,32632), ST_Transform(pt.coord,32632))<100 and Not ST_IsEmpty(ST_Intersection(ST_MakeLine(ST_ClosestPoint(hg.segment, pt.coord),pt.coord),ln.segment))
              order by dpthg asc';


    --execute sql1 into res1 using NEW.id;
    execute sql3 into res1 using new.device_id, new.ts;
    get diagnostics nrows = row_count;

    if (nrows = 0) then
       -- il punto è esterno al poligono: le coordinate non vengono modificate
      punto_su_linea := new.coord;
    else
      -- il punto è interno ad un poligono
      punto_su_linea := res1.punto_su_linea;
    end if;

    -- cerco se esiste già il punto nella tabella di correzione
    execute sql2 using new.device_id, new.ts;
    get diagnostics nrows = row_count;

    if nrows = 0 then
      insert into public.pose_pt_gnss(device_id, ts, user_email, time_zone, location, latitude, longitude, coord, accessibility_level)
      values (new.device_id, new.ts, new.user_email, new.time_zone, new.location, ST_Y(punto_su_linea), ST_X(punto_su_linea), punto_su_linea, new.accessibility_level);
    else
      update public.pose_pt_gnss set coord=punto_su_linea where device_id=new.device_id and ts=new.ts;
    end if;
    return null;
  end;
$$ language 'plpgsql';

drop trigger pose_pt_correction on public.pose_pt;

create trigger pose_pt_correction
after insert or update of coord on public.pose_pt
for each row execute procedure public.pose_building_correction();
