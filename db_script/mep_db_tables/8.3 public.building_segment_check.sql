﻿create or replace function public.building_segment_mainteiner() returns trigger as $$

begin
delete from public.building_segment
--select count(*) FROM public.building_segment
where id in (select id 
              from (select id,
                             ROW_NUMBER() over (partition by segment order by id) as rnum
                     from public.building_segment) t
              where t.rnum > 1);
return new;
end;
$$ language 'plpgsql';

drop trigger building_segment_unique on public.building_segment;

create trigger building_segment_unique
after insert or update on public.building_segment for each row
execute procedure public.building_segment_mainteiner();