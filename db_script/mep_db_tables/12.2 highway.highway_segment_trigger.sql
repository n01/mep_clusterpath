﻿create trigger highway_segment_trig 
after insert or update on highway.lines for each row
execute procedure highway.update_highway_segment();