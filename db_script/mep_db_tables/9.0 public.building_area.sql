﻿create table public.building_area
(	id serial not null,		-- The id of the row
	building_id bigint not null,	-- The id of the building
	name character varying,		-- The name
	area geometry,			-- The shape of the building
	primary key (id)		-- The primary key is fixed to be the id(this may be changed in a better way)
);