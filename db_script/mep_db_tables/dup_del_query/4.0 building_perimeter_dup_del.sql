﻿DELETE FROM public.building_perimeter
--select count(*) FROM public.building_perimeter
WHERE id IN (SELECT id 
              FROM (SELECT id,
                             ROW_NUMBER() OVER (partition BY perimeter ORDER BY id) AS rnum
                     FROM public.building_perimeter) t
              WHERE t.rnum > 1);