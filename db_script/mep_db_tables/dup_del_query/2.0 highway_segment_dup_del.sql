﻿--select count(*) from public.highway_segment
delete from public.highway_segment
where highway_id in (select highway_id from (select highway_id, ROW_NUMBER() over (partition by segment order by id) as rnum
						  from public.highway_segment) as t
						  where t.rnum > 1);
