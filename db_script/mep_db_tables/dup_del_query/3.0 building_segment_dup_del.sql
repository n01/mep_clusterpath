﻿--DELETE FROM public.building_segment
select count(*) FROM public.building_segment
WHERE id IN (SELECT id 
              FROM (SELECT id,
                             ROW_NUMBER() OVER (partition BY segment ORDER BY id) AS rnum
                     FROM public.building_segment) t
              WHERE t.rnum > 1);