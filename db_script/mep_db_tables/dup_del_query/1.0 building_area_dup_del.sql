﻿DELETE FROM public.building_area
WHERE id IN (SELECT id 
              FROM (SELECT id,
                             ROW_NUMBER() OVER (partition BY area ORDER BY id) AS rnum
                     FROM public.building_area) t
              WHERE t.rnum > 1);