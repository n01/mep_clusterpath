﻿drop table public.type_gps_satellite_point;

create table public.type_gps_satellite_point(
  device_id varchar(16),
  ts bigint,
  time_zone timestamp,
  utc character varying(23),
  azimut double precision,
  elevation double precision,
  point point,
  pnr double precision,
  snr double precision
);