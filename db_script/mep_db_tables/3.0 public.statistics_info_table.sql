﻿CREATE TABLE public.statistics_info
(
    user_email CHAR(50), -- the user email
    device_id VARCHAR(16), -- device_id of the user
    image_capture CHAR(3), -- yes if image_capture available, no otherwise
    wifi CHAR(3), -- yes if wifi on, no otherwise
    mobile_data CHAR(3), -- yes if mobile data on, no otherwise
    acq_time TIME, -- the total acquisition time
    bat_cons INT, -- the percentage of the battery consuption
    chargin CHAR(3), -- yes if the device is charging, no otherwise
    mem_used CHAR(3), -- the total memory used by the current acquisition
    device_model CHAR(50), -- the model of the device
    android_version CHAR(50), -- the android version of the current device
    kernel_version CHAR(50),  -- the kernel version of the current device (Not always available)
    km_walk TIME, -- the km walked by the user
    total_path_length DOUBLE PRECISION, -- the total distance walked by the user
    PRIMARY KEY (user_email,device_id) -- primary key
);