DROP TABLE pycluster.kmeans;

CREATE TABLE pycluster.kmeans
(
    device_id VARCHAR(16), 		-- device_id of the user
    ts BIGINT, 				-- serialized timestamp
    latitude DOUBLE PRECISION, 		-- latitude of the point
    longitude DOUBLE PRECISION, 	-- longitude of the point
    tr_point GEOMETRY, 			-- trace points
    accessibility_level INT, 		-- accessibility level of the current point
    PRIMARY KEY ( device_id , ts ) 	-- primary key is the pair device_id and serialized timestamp
);
