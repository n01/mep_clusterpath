﻿create or replace function public.building_perimeter_mainteiner() returns trigger as $$

  begin
    delete from public.building_perimeter
    where id in (select id
                  from (select id, ROW_NUMBER() over (partition by perimeter order by id) as rnum
                         from public.building_perimeter) t
                  where t.rnum > 1);
    return new;
  end;
$$ language 'plpgsql';

drop trigger building_perimeter_unique on public.building_perimeter;

create trigger building_perimeter_unique
after insert or update on public.building_perimeter for each row
execute procedure public.building_perimeter_mainteiner();

