﻿create or replace function duplicate_point_trigger()
  returns trigger as $$
    begin
      -- Postgresql9.3 does not have 'on conflict do' then this kind of trigger, remove for 9.5 or later.
      --if exists(select * from public.type_gps_position_point where device_id = new.device_id and ts = new.ts) then
      --  update public.type_gps_position_point set tr_point=ST_SetSRID(ST_MakePoint(public.type_gps_position_point.longitude, public.type_gps_position_point.latitude), 4326);
      --end if;
      -- Till here.

      if exists(select * from public.cluster_queue where location = new.location) then
        update public.cluster_queue set cluster_repeat_flag=true, pt_num=(select count(*) from public.type_gps_position_point where location=new.location) where public.cluster_queue.location=new.location;
      else
        insert into public.cluster_queue(location, pt_num, priority, cluster_flag, cluster_timestamp, cluster_repeat_flag)
        values (new.location, (select count(*) from public.type_gps_position_point as gps where gps.location = new.location), 1, FALSE, (select clock_timestamp()), TRUE);
      end if;
--
--    insert into public.cluster_queue(location, pt_num, priority, cluster_flag, cluster_timestamp, cluster_repeat_flag)
--    values (new.location, 0, 1, FALSE, (select clock_timestamp()), TRUE)
--      on CONFLICT (location) do update set cluster_repeat_flag=true, pt_num=(select count(*) from public.type_gps_position_point where location=new.location) where public.cluster_queue.location = new.location;
      return new;

-- pt_num = (select count(*) from public.type_gps_position_point as gps where gps.location = new.location),
-- (select count(*) from public.type_gps_position_point as gps where gps.location = new.location)
--       if exists (select *
--                   from public.type_gps_position_point
--                   where ts = new.ts  and device_id = new.device_id) then
--         return null;
--       else
--         if (select count(*) from public.cluster_queue as cq where cq.clustered = true and cq.re_cluster = false) > 0 then
--               update public.cluster_queue
--               set re_cluster = true
--               where public.cluster_queue.location = new.location;
--         else
--               insert into public.cluster_queue(location, pt_num, priority, cluster_flag, cluster_timestamp, cluster_repeat_flag)
--               values(new.location, (select count(*) from public.pose_pt_gnss where cluster_queue.location = new.location), 1, FALSE, (select clock_timestamp()), TRUE);
--         end if;
--         return new;
--       end if;
    end;
  $$ language plpgsql;

drop trigger check_duplicate on public.type_gps_position_point;

create trigger check_duplicate
before insert or update on public.type_gps_position_point
for each row execute procedure public.duplicate_point_trigger();
