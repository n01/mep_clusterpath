create or replace view path_length_view as
  select row_number() over (order by st_makeline(coord)) as id, st_length(st_transform(st_geomfromtext(st_astext(st_makeline(coord)), 4326), 900913)) as length, st_makeline(coord) as path
  from public.pose_pt_gnss
  group by time_zone;