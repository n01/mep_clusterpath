drop table public.neuron_pt;

create table public.neuron_pt
(
    ts bigint, 				-- serialized timestamp
    time_zone timestamp, 		-- UTC
    location varchar(30), 		-- acquisition's location
    latitude double precision, 		-- neuron's latitude
    longitude double precision, 	-- neuron's longitude
    coord geometry, 			-- neuron's geometry 
    mu_x float,				-- mean on the x axis
    mu_y float, 			-- mean on the y axis
    sigma_x float,			-- variance on the x axis
    sigma_y float,			-- variance on the y axis
    theta float,        -- rotation angle
    pts_list text,			-- the list of the points related with the neuron 
    pts_num int,			-- number of associated point
    accessibility_level int, 		-- accessibility level of the current point
    primary key (ts)  			-- primary key is the pair device_id and serialized timestamp

);
