﻿create or replace function public.building_area_mainteiner() returns trigger as $$

begin
delete from public.building_area
where id in (select id 
              from (select id,
                             ROW_NUMBER() over (partition by area order by id) as rnum
                     from public.building_area) t
              where t.rnum > 1);
return new;
end;
$$ language 'plpgsql';

drop trigger building_area_trig on public.building_area;

create trigger building_area_trig
after insert or update on public.building_area for each row
execute procedure public.building_area_mainteiner();

