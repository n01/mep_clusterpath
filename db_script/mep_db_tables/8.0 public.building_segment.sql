﻿create table public.building_segment
(
	id serial not null,		-- The id of the row
	building_id bigint not null,	-- The id of the building
	name character varying,		-- The name of the building
	segment geometry,		-- The segment extracted from the building
	primary key(id)			-- The primary key of the table
);