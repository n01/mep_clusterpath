﻿create or replace function building.update_building_area() returns trigger as $$
  begin
  if exists(with
              tmp_tab(geom) as (select area from public.building_area)
            select * from tmp_tab
            where new.wkb_geometry = geom) then
    return null;

  else
    insert into public.building_area(building_id, name, area)
      select pline.ogc_fid as building_id, pline.name, pline.wkb_geometry as area
      from (select distinct ogc_fid, wkb_geometry, name from building.multipolygons where wkb_geometry = new.wkb_geometry) as pline;
    return null;
  end if;
  -- Ensure that no replicated geometries are into the database
  -- delete from public.building_area
  -- where id in (select id
  --               from (select id,
  --                              ROW_NUMBER() over (partition by area order by id) as rnum
  --                      from public.building_area) t
  --               where t.rnum > 1);
  end;
$$ language 'plpgsql';

drop trigger building_area_trig_update on building.multipolygons;

create trigger building_area_trig_update
after insert or update on building.multipolygons for each row
execute procedure building.update_building_area();