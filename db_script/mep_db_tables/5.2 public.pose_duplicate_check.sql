﻿create or replace function public.pose_pt_dup_check() returns trigger as $$
  
  begin
   if exists(select * from public.pose_pt where device_id = new.device_id and ts=new.ts) then
      return null;
   else
      return new;
   end if;
  end;
$$ language 'plpgsql';

drop trigger check_pose_pt_duplicates on pose_pt;

create trigger check_pose_pt_duplicates
before insert on public.pose_pt for each row
execute procedure public.pose_pt_dup_check();