﻿drop table highway.lines;

create table highway.lines
(
  ogc_fid serial not null,
  osm_id character varying,
  name character varying,
  highway character varying,
  waterway character varying,
  aerialway character varying,
  barrier character varying,
  man_made character varying,
  z_order integer,
  other_tags character varying,
  wkb_geometry geometry(LineString,4326),
  constraint lines_pkey primary key (ogc_fid)
);

drop table building.multipolygons;

create table building.multipolygons(
  ogc_fid serial NOT NULL,
  osm_id character varying,
  osm_way_id character varying,
  name character varying,
  type character varying,
  aeroway character varying,
  amenity character varying,
  admin_level character varying,
  barrier character varying,
  boundary character varying,
  building character varying,
  craft character varying,
  geological character varying,
  historic character varying,
  land_area character varying,
  landuse character varying,
  leisure character varying,
  man_made character varying,
  military character varying,
  "natural" character varying,
  office character varying,
  place character varying,
  shop character varying,
  sport character varying,
  tourism character varying,
  other_tags character varying,
  wkb_geometry geometry(MultiPolygon,4326),
  constraint multipolygons_pkey primary key (ogc_fid)
);

