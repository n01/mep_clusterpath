create or replace view clustered_cities_view as
  select location, count(*)
  from pose_pt_gnss
  group by location;
