﻿﻿create or replace function highway.update_highway_segment() returns trigger as $$

begin
insert into public.highway_segment(highway_id, osm_id, name, segment)
		select new.ogc_fid as highway_id, new.osm_id, new.name, ST_SetSRID(ST_MakeLine(sp,ep),4326) as segment
		from (select ST_AsText(ST_PointN(new.wkb_geometry, generate_series(1, ST_NPoints(new.wkb_geometry)-1))) as sp,
			     ST_AsText(ST_PointN(new.wkb_geometry, generate_series(2, ST_NPoints(new.wkb_geometry)  ))) as ep) as segment;
		--where segment not in (select segment from public.highway_segment);
return new;
end;
$$ language 'plpgsql';


