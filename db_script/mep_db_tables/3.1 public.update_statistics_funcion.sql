﻿CREATE OR REPLACE FUNCTION public.update_statistics()
  RETURNS trigger AS
$BODY$
  BEGIN
	IF EXISTS (	SELECT * 
			FROM public.statistics_info 
			WHERE statistics_info.user_email = NEW.user_email AND statistics_info.device_id = NEW.device_id )  
		THEN  UPDATE public.statistics_info SET statistics_info.total_path_length  = OLD.total_path_length + ST_Length(NEW.path)
			WHERE statistics_info.user_email = NEW.user_email AND statistics_info.device_id = NEW.device_id;
			RETURN NEW;
	ELSE
		INSERT INTO public.statistics_info(user_email, device_id, total_path_length) 
		VALUES(NEW.user_email, NEW.device_id, ST_Lenght(NEW.path));
		RETURN NEW;
	END IF;	
	EXCEPTION
		WHEN NO_DATA_FOUND THEN
			RETURN NULL;
		WHEN OTHERS THEN
			RETURN NULL;
  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION public.duplicate_point_trigger()
  OWNER TO postgres;