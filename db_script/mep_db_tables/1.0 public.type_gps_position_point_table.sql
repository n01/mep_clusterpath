﻿drop table public.type_gps_position_point;

create table public.type_gps_position_point(
    device_id varchar(16), -- device_id of the user
    ts bigint, -- serialized timestamp
    user_email char(50), -- the user email
    time_zone timestamp, -- timestamp
    location text, -- location of the acquisition
    latitude double precision, -- latitude of the point
    longitude double precision, -- longitude of the point
    tr_point geometry, -- trace points
    accessibility_level int, -- accessibility level of the current point
    primary key (device_id,ts) -- primary key is the pair device_id and serialized timestamp
);