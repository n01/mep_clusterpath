drop table public.mep_folders_table;

create table public.mep_folders_table(
  device_id varchar(16),
  item_name varchar(22),
  location text,
  user_email text,
  elaboration_dt date,
  elaboration_flag bool,
  primary key (device_id, item_name)
);