#!/bin/bash


# -u postgres psql mep_neural_test

# Requirements for the dica script to work
sudo add-apt-repository ppa:ubuntugis/ppa && apt-get update
sudo apt-get install gdal-bin

# `sudo -u postgres -i`
# Vars
# DB=mep_neural_test
DB=mep_db

createdb $DB
# createdb $DB2

# ======================= Table ========================================================================

GPSPP="﻿CREATE TABLE public.type_gps_position_point
(
    device_id VARCHAR(16), 		-- device_id of the user
    ts BIGINT, 				-- serialized timestamp
    user_email CHAR(50), 		-- the user email
    time_zone TIMESTAMP, 		-- timestamp
    location VARCHAR(30), 		-- location of the acquisition
    latitude DOUBLE PRECISION, 		-- latitude of the point
    longitude DOUBLE PRECISION, 	-- longitude of the point
    tr_point GEOMETRY, 			-- trace points
    accessibility_level INT, 		-- accessibility level of the current point
    PRIMARY KEY ( device_id , ts ) 	-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "$GPSPP"

GPSPPqry= "\dS+ type_gps_position_point;"

psql -d $DB -c "$GPSPPqry"

# ======================================================================================================
#
#                                Table "public.type_gps_position_point"
#       Column        |            Type             | Modifiers | Storage  | Stats target | Description
#---------------------+-----------------------------+-----------+----------+--------------+-------------
# device_id           | character varying(16)       | not null  | extended |              |
# ts                  | bigint                      | not null  | plain    |              |
# user_email          | character(50)               |           | extended |              |
# time_zone           | timestamp without time zone |           | plain    |              |
# location            | character varying(30)       |           | extended |              |
# latitude            | double precision            |           | plain    |              |
# longitude           | double precision            |           | plain    |              |
# tr_point            | geometry                    |           | main     |              |
# accessibility_level | integer                     |           | plain    |              |
#Indexes:
#    "type_gps_position_point_pkey" PRIMARY KEY, btree (device_id, ts)
#Triggers:
#    check_duplicate BEFORE INSERT ON type_gps_position_point FOR EACH ROW EXECUTE PROCEDURE duplicate_point_trigger()

# ======================= Schema =======================================================================

BUISCHEMA="CREATE SCHEMA building
  AUTHORIZATION postgres;"

psql -d $DB -c "$BUISCHEMA"

# ======================= Schema =======================================================================

HIGHSCHEMA="CREATE SCHEMA highway
  AUTHORIZATION postgres;"

psql -d $DB -c "$HIGHSCHEMA"

# ======================= Table ========================================================================

GPSDtest="create table public.gpsdata_test
(
    id integer,
    device_id varchar(16), 		-- device_id of the user
    ts bigint, 				-- serialized timestamp
    user_email char(50), 		-- the user email
    time_zone timestamp, 		-- timestamp
    location varchar(30), 		-- location of the acquisition
    latitude double precision, 		-- latitude of the point
    longitude double precision, 	-- longitude of the point
    coord geometry, 			-- corrected points
    accessibility_level int, 		-- accessibility level of the current point
    "time" time without time zone,
    primary key (id)  			-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "$GPSDtest"

GPSDqry= "\dS+ gpsdata_test;"

psql -d $DB -c "$GPSDqry"

# ======================================================================================================
#
#                                      Table "public.gpsdata_test"
#       Column        |            Type             | Modifiers | Storage  | Stats target | Description
#---------------------+-----------------------------+-----------+----------+--------------+-------------
# id                  | integer                     | not null  | plain    |              |
# device_id           | character varying(16)       |           | extended |              |
# ts                  | bigint                      |           | plain    |              |
# user_email          | character(50)               |           | extended |              |
# time_zone           | timestamp without time zone |           | plain    |              |
# location            | character varying(30)       |           | extended |              |
# latitude            | double precision            |           | plain    |              |
# longitude           | double precision            |           | plain    |              |
# coord               | geometry                    |           | main     |              |
# accessibility_level | integer                     |           | plain    |              |
# time                | time without time zone      |           | plain    |              |
# Indexes:
#    "gpsdata_test_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

GPSDtest_corr="create table public.gpsdata_test_corr
(
    id varchar(20),
    device_id varchar(16), 	-- device_id of the user
    ts bigint, 			-- serialized timestamp
    user_email char(50), 	-- the user email
    time_zone timestamp, 	-- timestamp
    location varchar(30), 	-- location of the acquisition
    latitude double precision, 	-- latitude of the point
    longitude double precision, -- longitude of the point
    coord geometry, 		-- corrected points
    accessibility_level int, 	-- accessibility level of the current point
    "time" time without time zone,
    primary key (id)  		-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "$GPSDtest_corr"

GPSDqryc= "\dS+ gpsdata_test_corr;"

psql -d $DB -c "$GPSDqryc"

# ======================================================================================================
#
#                                   Table "public.gpsdata_test_corr"
#       Column        |            Type             | Modifiers | Storage  | Stats target | Description
#---------------------+-----------------------------+-----------+----------+--------------+-------------
# id                  | integer                     |           | plain    |              |
# device_id           | character varying(16)       |           | extended |              |
# ts                  | bigint                      |           | plain    |              |
# user_email          | character(50)               |           | extended |              |
# time_zone           | timestamp without time zone |           | plain    |              |
# location            | character varying(30)       |           | extended |              |
# latitude            | double precision            |           | plain    |              |
# longitude           | double precision            |           | plain    |              |
# coord               | geometry                    |           | main     |              |
# accessibility_level | integer                     |           | plain    |              |
# time                | time without time zone      |           | plain    |              |


# ======================= Table ========================================================================

BUPER="﻿create table public.build_perimiter
(	id integer not null,	-- The id of the row
	name character varying,	-- The name
	line geometry,		-- The shape of the building
	primary key (id)	-- The primary key is fixed to be the id(this may be changed in a better way)
);"

psql -d $DB -c "$BUPER"

BUPERqry= "\dS+ build_perimiter;"

psql -d $DB -c "$BUPERqry"

# ======================================================================================================
#
#                         Table "public.build_perimiter"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# line   | geometry          |           | main     |              |
#Indexes:
#    "build_perimiter_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

BUSEG="create table public.build_segment
(
	id integer,
	name character varying,
	seg geometry,
	idpoly integer,
	primary key(id)
);"

psql -d $DB -c "$BUSEG"

BUSEGqry= "\dS+ build_segment;"

psql -d $DB -c "$BUSEGqry"

# ======================================================================================================
#
#                          Table "public.build_segment"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# seg    | geometry          |           | main     |              |
# idpoly | integer           |           | plain    |              |
#Indexes:
#    "build_segment_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

BUILDING="create table public.building
(
	id integer,
	name character varying,
	polyg geometry,
	primary key(id)
);"

psql -d $DB -c "$BUILDING"

BUILDINGqry= "\dS+ building;"

psql -d $DB -c "$BUILDINGqry"

# ======================================================================================================
#
#                            Table "public.building"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# polyg  | geometry          |           | main     |              |
#Indexes:
#    "building_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

HIGHSEG="create table public.highway_segment
(
	id integer,
	name character varying,
	seg geometry,
	primary key(id)
);"

psql -d $DB -c "$HIGHSEG"

HIGHSEGqry= "\dS+ highway_segment;"

psql -d $DB -c "$HIGHSEGqry"

# ======================================================================================================
#
#                         Table "public.highway_segment"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# seg    | geometry          |           | main     |              |
#Indexes:
#    "highway_segment_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================
# ======================= Table ========================================================================

PYCLUSTERSCHEMA = "create schema pycluster authorization postgres;"

psql -d $DB -c "PYCLUSTERSCHEMA"

PYCLUSTERDB = "CREATE TABLE pycluster.dbscan
(
    device_id VARCHAR(16), 		-- device_id of the user
    ts BIGINT, 				-- serialized timestamp
    latitude DOUBLE PRECISION, 		-- latitude of the point
    longitude DOUBLE PRECISION, 	-- longitude of the point
    tr_point GEOMETRY, 			-- trace points
    accessibility_level INT, 		-- accessibility level of the current point
    PRIMARY KEY ( device_id , ts ) 	-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "PYCLUSTERDB"

PYCLUSTERKMEANS = "CREATE TABLE pycluster.kmeans
(
    device_id VARCHAR(16), 		-- device_id of the user
    ts BIGINT, 				-- serialized timestamp
    latitude DOUBLE PRECISION, 		-- latitude of the point
    longitude DOUBLE PRECISION, 	-- longitude of the point
    tr_point GEOMETRY, 			-- trace points
    accessibility_level INT, 		-- accessibility level of the current point
    PRIMARY KEY ( device_id , ts ) 	-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "PYCLUSTERKMEANS"

PYCLUSTEROPTICS = "CREATE TABLE pycluster.optics
(
    device_id VARCHAR(16), 		-- device_id of the user
    ts BIGINT, 				-- serialized timestamp
    latitude DOUBLE PRECISION, 		-- latitude of the point
    longitude DOUBLE PRECISION, 	-- longitude of the point
    tr_point GEOMETRY, 			-- trace points
    accessibility_level INT, 		-- accessibility level of the current point
    PRIMARY KEY ( device_id , ts ) 	-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "PYCLUSTEROPTICS"

LIMIT = "CREATE TABLE public.limit
(
    min BIGINT
);"

psql -d $DB -c "LIMIT"
