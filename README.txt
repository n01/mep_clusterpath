
======================================= MEP-CLUSTERPATH ================================================================

author: Andrea Peccini
date: 10-11-2016

The mep-clusterpath algorithm needs some further elements to be compiled and installed. Fortunately the execution of the
mep-clusterpath.sh script should solve many problems, offering the support for a correct installation.

./mep-clusterpath.sh

or

sh mep-clusterpath.sh

In any case the execution of the script require the super user permission then it should be executed as the root user,
due to the fact that some extra package should be installed on the system.
The system was heavily test on ubuntu 16.04 LTS, with python 3.5. No support for other linux distribution mainly due to 
the mep-fusion algorithm dependence library.

The mep-clusterpath backend simply start by executing the program

init.sh

into the mep-python folder. Before running it remember to set the config.txt file in order to sync the program with 
the proper database informations.

Good Luck.

Andrea
