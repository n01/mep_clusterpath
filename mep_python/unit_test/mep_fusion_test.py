#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import unittest
import datetime
from ModelModule.FusionManager import  fusion_handler
import os
path = "/home/anonymous/workspace/python3.5.2/mep-clusteringpath/mep_python/trace/emanuele.debernardi@polimi.it" \
       "/96763cfa8a51187e/Orta_San_Giulio/MEP_1472381886091"


class FusionHandler(unittest.TestCase):

    def test_fusion(self):
        mt_inf_crt = os.path.join(path, 'tmp/MEP_MOTION_INFO_CORRECTED.txt')
        if os.path.isfile(mt_inf_crt):
            os.remove(mt_inf_crt)
        t_start = datetime.datetime.now()
        fusion_handler(path)
        t_end = datetime.datetime.now()
        delta = t_end - t_start
        print('Delta time: %s' % delta)
        # esecuzione 11/11/1016 delta = 0:02:14.418965
        self.assertLessEqual(delta, datetime.timedelta(minutes=3))


if __name__ == '__main__':
    unittest.main()
