#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import unittest
import numpy as np
from ModelModule.CoordConverter import *
lla_ref = np.array([4547.814153000000260, 824.7567880000000287, 354.0])
lla_pt_dms = np.array([4547.813401, 824.759687, 349.0])
lla_pt_dd = np.array([45.79689002, 8.41266145, 349.])


class TestGeoConversionMethod(unittest.TestCase):

    def test_ECEF2LLA_dd(self):
        ecef = LLA2ECEF(lla_pt_dd, 'dd')
        dms = np.array(ECEF2LLAdms(ecef=ecef))
        out = np.array([np.linalg.norm(i) for i in lla_pt_dd[0:2] - dms[0:2]])
        print("maximum error: %s" % max(out))
        self.assertLess(max(out), 0.001)

    def test_ECEF2LLA_dms(self):
        ecef = LLA2ECEF(lla_pt_dms, 'dms')
        dms = np.array(ECEF2LLAdms(ecef=ecef))
        out = np.array([np.linalg.norm(i) for i in lla_pt_dd[0:2] - dms[0:2]])
        print("maximum error: %s" % max(out))
        self.assertLess(max(out), 0.001)

    def test_ECEF_wrong_type(self):
        ecef = LLA2ECEF(lla_pt_dd, 'aksjhde')
        self.assertEqual(ecef, "The required data type is not available!")

    def test_ENU2LLA_dd(self):
        enu = LLA2ENU(lla_pt_dd, lla_ref, 'dd', 'dms')
        lla = ENU2LLA(enu, lla_ref, 'dms')
        out = np.array([np.linalg.norm(i) for i in lla_pt_dd[0:2] - lla[0:2]])
        print("maximum error: %s" % max(out))
        self.assertLess(max(out), 0.001)

    def test_ENU2LLA_dms(self):
        enu = LLA2ENU(lla_pt_dms, lla_ref, 'dms', 'dms')
        lla = ENU2LLA(enu, lla_ref, 'dms')
        out = np.array([np.linalg.norm(i) for i in lla_pt_dd[0:2] - lla[0:2]])
        print("maximum error: %s" % max(out))
        self.assertLess(max(out), 0.001)

    def test_LLA2ENU_wrong_type(self):
        enu = LLA2ENU(lla_pt_dms, lla_ref, 'das', 'dasd')
        self.assertEqual(enu, "The required data type is not available!")

    def test_ENU2LLA_(self):
        enu = LLA2ENU(lla_pt_dms, lla_ref, 'dms', 'dms')
        lla = ENU2LLA(enu, lla_ref, 'asdsa')
        self.assertEqual(lla, "The required data type is not available!")


if __name__ == '__main__':
    unittest.main()
