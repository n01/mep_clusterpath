#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

from mdp import graph, numx
from builtins import map
from AING.GraphObjects import NGEdgeData, SampleEdgeData, NGNodeData, SampleNodeData


class ExtendedGraphNode(graph.GraphNode):
    """Represent a graph node and all information attached to it."""

    def __init__(self, data=None, esamp=None):

        super(ExtendedGraphNode).__init__()

        self.data = data
        # edges in
        self.ein = []
        # edges out
        self.eout = []
        # edges to sample
        self.e2samp = []

        # edges out to sample
        # if isinstance(data, NGNodeData):
        #     self.esamp = []

    def add_edge_in(self, edge):
        self.ein.append(edge)

    def add_edge_out(self, edge):
        if isinstance(edge.data, NGEdgeData):
            self.eout.append(edge)
        elif isinstance(edge.data, SampleEdgeData):
            self.e2samp.append(edge)

    def remove_edge_in(self, edge):
        self.ein.remove(edge)

    def remove_edge_out(self, edge):
        self.eout.remove(edge)

    def get_edges_in(self, from_=None):
        """Return a copy of the list of the entering edges. If from_
        is specified, return only the nodes coming from that node."""
        inedges = self.ein[:]
        if from_:
            inedges = [edge for edge in inedges if edge.head == from_]
        return inedges

    def get_edges_out(self, to_=None):
        """Return a copy of the list of the outgoing edges. If to_
        is specified, return only the nodes going to that node."""
        outedges = self.eout[:]
        if to_:
            outedges = [edge for edge in outedges if edge.tail == to_]
        return outedges

    def get_edges(self, neighbor=None):
        """Return a copy of all edges. If neighbor is specified, return
        only the edges connected to that node."""
        return self.get_edges_in(from_=neighbor) + self.get_edges_out(to_=neighbor)

    def in_degree(self):
        """Return the number of entering edges."""
        return len(self.ein)

    def out_degree(self):
        """Return the number of outgoing edges."""
        return len(self.eout)

    def degree(self):
        """Return the number of edges."""
        return self.in_degree()+self.out_degree()

    def get_in_neighbors(self):
        """Return the neighbors down in-edges (i.e. the parents nodes)."""
        return [x.get_head() for x in self.ein]

    def get_out_neighbors(self):
        """Return the neighbors down in-edges (i.e. the parents nodes)."""
        return [x.get_tail() for x in self.eout]

    def get_neighbors(self):
        return self.in_neighbors() + self.out_neighbors()

    def get_samples(self):
        if isinstance(self.data, NGNodeData):
            return [x.get_tail() for x in self.e2samp]
        elif isinstance(self.data, SampleNodeData):
            errstr = 'A sample cannot have link to other samples'
            raise graph.GraphException(errstr)


class ExtendedGraphEdge(graph.GraphEdge):
    """Represent a graph edge and all information attached to it."""

    def __init__(self, head, tail, data=None, length=0):

        super(ExtendedGraphEdge, self).__init__(head, tail, data)

        # head node
        self.head = head
        # neighbors out
        self.tail = tail
        # arbitrary data slot
        self.data = data
        # length of the edge
        self.length = length

    def get_ends(self):
        """Return the tuple (head_id, tail_id)."""
        return self.head, self.tail

    def get_tail(self):
        return self.tail
    #
    # def get_head(self):
    #     return self.head

    # def get_length(self):
    #     return self.length


class ExtendedGraph(graph.Graph):
    """Override the basic mdp.graph.Graph class in order to add the concept of SampleNodeData and SampleEdgeData needed
    in order to keep memory of the data_points connected to a neuron. NGNodeData(i.e. the neurons) are connected each
    other with NGEdgeData(i.e. the usual edges), while the SampleNodeData(i.e. the data points) are connected to the
    respective NGNodeData through SampleEdgeData(i.e. the new type of edges). The ExtendedGraph keep track of the two
    different topology structure and keeps them into two different structures(i.e. neuron_nodes-neuron_edges and
     sample_nodes-sample_edges).
    """

    # @ Overrides
    def __init__(self):
        """
        Constructor of the ExtendedGraph object
        """
        super(ExtendedGraph).__init__()
        # 1) ====> List of neuron nodes
        self.neuron_nodes = []
        # 2) ====> List of neuron edges
        self.neuron_edges = []
        # 3) ====> List of samples node
        self.sample_nodes = []
        # 4) ====> List of sample edges
        self.sample_edges = []

    # @Overrides
    def add_node(self, data=None):
        """
        Add a node to the graph.
        :param data: input node
        :return: the created node
        """
        # 1) ====> Create the new node
        node = ExtendedGraphNode(data=data)
        # 2) ====> Instance check
        if isinstance(data, NGNodeData):
            # 2.1) >>>> If the node is a neuron add the neuron nodes list
            self.neuron_nodes.append(node)
        elif isinstance(data, SampleNodeData):
            # 2.2) >>>> If the node is a sample add the sample nodes list
            self.sample_nodes.append(node)
        return node

    def remove_node(self, node):
        # the node is not in this graph
        if node not in self.neuron_nodes:
            errstr = 'This node is not part of the graph (%s)' % node
            raise graph.GraphException(errstr)

        # remove all edges containing this node
        for edge in node.get_edges():
            self.remove_edge(edge)
        # remove the node
        self.neuron_nodes.remove(node)

    # @Overrides
    def add_edge(self, head, tail, data=None):
        """
        Add an edge going from head to tail.
        :param head: head node
        :param tail: tail node
        :param data: input edge
        :return: the created edge
        """
        # 1) ====> Create the new edge
        edge = ExtendedGraphEdge(head, tail, data=data)
        # 2) ====> Set the length of the edge
        # edge.length = np.linalg.norm(head.data.pos - tail.data.pos)
        # 3) ====> Update head and tail node with the newly created edge
        head.add_edge_out(edge)
        tail.add_edge_in(edge)
        # 4) ====> Instance check
        if isinstance(data, NGEdgeData):
            # 4.1) >>>> Add to the edges dictionary
            self.neuron_edges.append(edge)
        elif isinstance(data, SampleEdgeData):
            # 4.2) >>>> Add to the edges dictionary
            self.sample_edges.append(edge)
        return edge

    def remove_edge(self, edge):
        head, tail = edge.get_ends()
        # remove from head
        head.remove_edge_out(edge)
        # remove from tail
        tail.remove_edge_in(edge)
        # remove the edge
        self.neuron_edges.remove(edge)

    # ==== populate functions
    def add_nodes(self, data):
        """Add many nodes at once.

        data -- number of nodes to add or sequence of data values, one for
                each new node"""
        if not graph.is_sequence(data):
            data = [None]*data
        return list(map(self.add_node, data))

    def add_tree(self, tree):
        """Add a tree to the graph.

        The tree is specified with a nested list of tuple, in a LISP-like
        notation. The values specified in the list become the values of
        the single nodes.

        Return an equivalent nested list with the nodes instead of the values.

        Example:
        a=b=c=d=e=None
        g.add_tree( (a, b, (c, d ,e)) )
        corresponds to this tree structure, with all node values set to None:

                a
               / \
              b   c
                 / \
                d   e
        """

        def _add_edge(root, son):
            self.add_edge(root, son)
            return root

        nodes = graph.recursive_map(self.add_node, tree)
        graph.recursive_reduce(_add_edge, nodes)
        return nodes

    def add_full_connectivity(self, from_nodes, to_nodes):
        """Add full connectivity from a group of nodes to another one.
        Return a list of lists of edges, one for each node in 'from_nodes'.

        Example: create a two-layer graph with full connectivity.
        g = Graph()
        layer1 = g.add_nodes(10)
        layer2 = g.add_nodes(5)
        g.add_full_connectivity(layer1, layer2)
        """
        edges = []
        for from_ in from_nodes:
            edges.append([self.add_edge(from_, x) for x in to_nodes])
        return edges

    # ==== graph algorithms
    def topological_sort(self):
        """Perform a topological sort of the nodes. If the graph has a cycle,
        throw a GraphTopologicalException with the list of successfully
        ordered nodes."""
        # topologically sorted list of the nodes (result)
        topological_list = []
        # queue (fifo list) of the nodes with in_degree 0
        topological_queue = []
        # {node: in_degree} for the remaining nodes (those with in_degree>0)
        remaining_indegree = {}

        # init queues and lists
        for node in self.neuron_nodes:
            indegree = node.in_degree()
            if indegree == 0:
                topological_queue.append(node)
            else:
                remaining_indegree[node] = indegree

        # remove nodes with in_degree 0 and decrease the in_degree of their sons
        while len(topological_queue):
            # remove the first node with degree 0
            node = topological_queue.pop(0)
            topological_list.append(node)
            # decrease the in_degree of the sons
            for son in node.out_neighbors():
                remaining_indegree[son] -= 1
                if remaining_indegree[son] == 0:
                    topological_queue.append(son)

        # if not all nodes were covered, the graph must have a cycle
        # raise a GraphTopographicalException
        if len(topological_list) != len(self.neuron_nodes):
            raise graph.GraphTopologicalException(topological_list)

        return topological_list

    # ==== Depth-First sort
    def _dfs(self, neighbors_fct, root, visit_fct=None):
        # core depth-first sort function
        # changing the neighbors function to return the sons of a node,
        # its parents, or both one gets normal dfs, reverse dfs, or
        # dfs on the equivalent undirected graph, respectively

        # result list containing the nodes in Depth-First order
        dfs_list = []
        # keep track of all already visited nodes
        visited_nodes = { root: None }

        # stack (lifo) list
        dfs_stack = []
        dfs_stack.append(root)

        while len(dfs_stack):
            # consider the next node on the stack
            node = dfs_stack.pop()
            dfs_list.append(node)
            # visit the node
            if visit_fct != None:
                visit_fct(node)
            # add all sons to the stack (if not already visited)
            for son in neighbors_fct(node):
                if son not in visited_nodes:
                    visited_nodes[son] = None
                    dfs_stack.append(son)

        return dfs_list

    def dfs(self, root, visit_fct=None):
        """Return a list of nodes in some Depth First order starting from
        a root node. If defined, visit_fct is applied on each visited node.

        The returned list does not have to contain all nodes in the
        graph, but only the ones reachable from the root.
        """

        neighbors_fct = lambda node: node.out_neighbors()
        return self._dfs(neighbors_fct, root, visit_fct=visit_fct)

    def undirected_dfs(self, root, visit_fct=None):
        """Perform Depth First sort.

        This function is identical to dfs, but the sort is performed on
        the equivalent undirected version of the graph."""
        neighbors_fct = lambda node: node.neighbors()
        return self._dfs(neighbors_fct, root, visit_fct=visit_fct)

    # ==== Connected components
    def connected_components(self):
        """Return a list of lists containing the nodes of all connected
        components of the graph."""

        visited = {}

        def visit_fct(node, visited=visited):
            visited[node] = None

        components = []
        nodes = self.neuron_nodes
        for node in nodes:
            if node in visited:
                continue
            components.append(self.undirected_dfs(node, visit_fct))
        return components

    def is_weakly_connected(self):
        """Return True if the graph is weakly connected."""
        return len(self.undirected_dfs(self.neuron_nodes[0])) == len(self.neuron_nodes)

    # ==== Breadth-First Sort
    # BFS and DFS could be generalized to one function. I leave them
    # distinct for clarity.
    def _bfs(self, neighbors_fct, root, visit_fct=None):
        # core breadth-first sort function
        # changing the neighbors function to return the sons of a node,
        # its parents, or both one gets normal bfs, reverse bfs, or
        # bfs on the equivalent undirected graph, respectively

        # result list containing the nodes in Breadth-First order
        bfs_list = []
        # keep track of all already visited nodes
        visited_nodes = { root: None }

        # queue (fifo) list
        bfs_queue = []
        bfs_queue.append(root)

        while len(bfs_queue):
            # consider the next node in the queue
            node = bfs_queue.pop(0)
            bfs_list.append(node)
            # visit the node
            if visit_fct != None:
                visit_fct(node)
            # add all sons to the queue (if not already visited)
            for son in neighbors_fct(node):
                if son not in visited_nodes:
                    visited_nodes[son] = None
                    bfs_queue.append(son)

        return bfs_list

    def bfs(self, root, visit_fct=None):
        """Return a list of nodes in some Breadth First order starting from
        a root node. If defined, visit_fct is applied on each visited node.

        Note the returned list does not have to contain all nodes in the
        graph, but only the ones reachable from the root."""

        neighbors_fct = lambda node: node.out_neighbors()
        return self._bfs(neighbors_fct, root, visit_fct=visit_fct)

    def undirected_bfs(self, root, visit_fct=None):
        """Perform Breadth First sort.

        This function is identical to bfs, but the sort is performed on
        the equivalent undirected version of the graph."""

        neighbors_fct = lambda node: node.neighbors()
        return self._bfs(neighbors_fct, root, visit_fct=visit_fct)

