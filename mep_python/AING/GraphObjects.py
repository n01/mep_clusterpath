#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

from builtins import object


class NGNodeData(object):
    """Data associated to a node in a Growing Neural Gas graph."""
    def __init__(self, pos, init_th=100,  error=0.0, hits=0, label='3'):
        # reference vector (spatial position)
        self.pos = pos
        # cumulative error
        self.th = init_th
        self.label = label

        self.cum_error = error
        self.hits = hits


class NGEdgeData(object):
    """Data associated to an edge in a Growing Neural Gas graph."""

    def __init__(self, age=0):
        self.age = age


class SampleNodeData(object):
    """Data points associated to a Gr """
    def __init__(self, pos):
        self.pos = pos


class SampleEdgeData(object):

    def __init__(self, age=0):
        self.age = age

    def inc_age(self):
        self.age += 1
