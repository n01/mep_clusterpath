#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import numpy as np
from AING.ExtendedGraph import ExtendedGraph
from mdp import numx, numx_rand, Node
from AING.GraphObjects import *


def _move_node(wy_tilde, wy, eps):
    """Move a the wy_tild node by eps in the direction wy."""
    wy_tilde.data.pos += eps * (wy - wy_tilde.data.pos)


def _get_nearest_nodes(g, x):
    """Return the two nodes in the graph that are nearest to x and their
    squared distances. (Return ([node1, node2], [dist1, dist2])"""
    inf, sup = 0, 2
    if isinstance(x.data, NGNodeData):
        distances = np.array(list(map(lambda j: np.linalg.norm(j.data.pos - x.data.pos), g.neuron_nodes)))
        if x in g.neuron_nodes:
            inf, sup = 1, 3
        ids = distances.argsort()[inf:sup]
    else:
        # distances of all graph nodes from x
        distances = np.array(list(map(lambda j: np.linalg.norm(j.data.pos - x), g.neuron_nodes)))
        ids = distances.argsort()[inf:sup]
    return (g.neuron_nodes[ids[0]], g.neuron_nodes[ids[1]]), distances.take(ids)


def _get_nearest_neighbor(g, x):
    """Return the node in the graph that is nearest to x and its
    squared distance. (Return ([node], [dist])"""
    idx = 0
    if isinstance(x.data, NGNodeData):
        distances = np.array(list(map(lambda j: np.linalg.norm(j.data.pos - x.data.pos), g.neuron_nodes)))
        if x in g.neuron_nodes:
            idx = 1
        ids = distances.argsort()[0:2]
    else:
        distances = np.array(list(map(lambda j: np.linalg.norm(j.data.pos - x), g.neuron_nodes)))
        ids = distances.argsort()[0:2]
    return g.neuron_nodes[ids[idx]], distances.take(ids)


class AdaptiveIncrementalNeuralGasNode(Node):
    """

    """
    def __init__(self, init=None, max_age=2147483647, d=0.095, d_min=1.0e-10, max_nodes=2147483647, input_dim=None, dtype=None):

        (self.max_age, self.d, self.d_min, self.max_nodes) = (max_age, d, d_min, max_nodes)

        super(AdaptiveIncrementalNeuralGasNode, self).__init__(input_dim, None, dtype)

        self.tlen, self.k, self.k1, self.k2 = 0, 0, self.d, self.d_min

        self.graph = ExtendedGraph()

        self.get_nearest_distance = lambda it: sorted(list(map(lambda j: np.linalg.norm(j.data.pos - it.data.pos), self.graph.neuron_nodes)))[1]

        if init is not None:
            if self.dtype is None:
                self.dtype = init[0].dtype

            node1, node2 = self.graph.add_node(NGNodeData(init[0])), self.graph.add_node(NGNodeData(init[1]))

            self.graph.add_edge(node1, node2, NGEdgeData())

            map(self._update_threshold, [node1, node2])

            self.graph.add_edge(node1, self.graph.add_node(SampleNodeData(init[0])), SampleEdgeData())
            self.graph.add_edge(node2, self.graph.add_node(SampleNodeData(init[1])), SampleEdgeData())

    def _set_input_dim(self, n):
        self._input_dim = n
        self.output_dim = n

    # def _remove_old_edges(self, edges):
    #     """Remove all edges older than the maximal age."""
    #     max_age = self.max_age
    #     for edge in edges:
    #         if edge.data.age > max_age:
    #             self.graph.remove_edge(edge)

    def _get_nearest_node_distance(self, item):
        """
        Get the nearest node distance with respect to the input item.
        :param item:
        :return:
        """
        return sorted(list(map(lambda j: np.linalg.norm(j.data.pos - item.data.pos), self.graph.neuron_nodes)))[1]

    def _get_nearest_neighbor(self, x):
        """Return the node in the graph that is nearest to x and its squared distance.
        :param x: the current input point.
        :return: the nearest neighbor and its distance.
        """
        dist = lambda nn: np.linalg.norm(x - nn.data.pos)

        if isinstance(x.data, NGNodeData):
            x = x.data.pos
        print(list(map(dist, self.graph.neuron_nodes)))
        distances = np.array(list(map(dist, self.graph.neuron_nodes)))
        ids = distances.argsort()[0:2]
        return self.graph.neuron_nodes[ids[1]], distances.take(ids)

    def _get_nearest_nodes(self, x):
        """Return the two nodes in the graph that are nearest to x and their
        squared distances. (Return ([node1, node2], [dist1, dist2])"""

        _dist = lambda nn: np.linalg.norm(nn.data.pos - x)

        if isinstance(x.data, NGNodeData):
            x = np.array(x.data.pos)
        # distances = np.array(list(map(_dist, self.graph.neuron_nodes)))
        ids = np.array(list(map(_dist, self.graph.neuron_nodes))).argsort()[0:2]
        return self.graph.neuron_nodes[ids[0]], self.graph.neuron_nodes[ids[1]]

    def _update_threshold(self, n0):
        """
        The threshold is computed accordingly to the expression that can be found on "An Adaptive Incremental Clustering
        Method Based on the Growing Neural Gas Algorithm" by Mohamed-Rafik Bouguelia, Yolande Belaid, Abdel Belaid.
        :param n0: the node upon which the update must be performed
        :return: None
        """
        # Xn is the linked samples set, Vn is the linked neuron set
        Xn_edg, Vn_edg = n0.get_samples(), n0.get_edges()

        dist2n0 = lambda kk: np.linalg.norm(n0.data.pos - kk.data.pos)
        # dist2neigh = lambda pp: len(pp.tail.esamp) * dist2n0(pp.tail) # Deprecated

        if len(Xn_edg) > 0 or sum([len(ii.tail.get_samples()) for ii in Vn_edg]) > 0:

            # term1 = sum(list(map(dist2n0, Xn_edg)))
            term1 = sum([dist2n0(ii) for ii in Xn_edg])

            # term2 = sum(list(map(dist2neigh, Vn_edg)))
            # term2 = sum([len(ii.tail.esamp) * ii.length for ii in Vn_edg]) # Deprecated
            term2 = sum([len(ii.tail.get_samples()) * dist2n0(ii.tail) for ii in Vn_edg])

            n0.data.th = (term1 + term2) / (len(Xn_edg) + sum([len(ii.tail.get_samples()) for ii in Vn_edg]))
        else:
            y_tilde, dists_cl = _get_nearest_neighbor(self.graph, n0)
            n0.data.th = dist2n0(y_tilde) / 2.
            # n0.data.th = (np.linalg.norm(n0.data.pos - y_tilde.data.pos)) / 2.  # Deprecated
        return

    def _train(self, input_stream):
        """

        :param input_stream:
        :return:
        """
        def _remove_old_edges():
            """Remove all edges older than the maximal age."""
            max_age = self.max_age
            for edge in self.graph.neuron_edges:
                if edge.data.age > max_age:
                    self.graph.remove_edge(edge)

        # k1 = self.d
        # k2 = self.d_min
        # k2 = self.d

        dist_samp2node = lambda pt, nd: np.linalg.norm(pt - nd.data.pos)

        # 0) ====> If missing, generate two initial nodes at random assuming that the input data has zero mean and unit
        #          variance, choose the random position according to a gaussian distribution with zero mean and unit
        #          variance
        if len(self.graph.neuron_nodes) == 0:
            normal = numx_rand.normal
            self.graph.add_node(NGNodeData(self._refcast(normal(0.0, 1.0, self.input_dim))))
            self.graph.add_node(NGNodeData(self._refcast(normal(0.0, 1.0, self.input_dim))))

        # 1) ====> While some data-points remain unread: get next data-point x
        for x in input_stream:

            # 2) ====> Find the nearest nodes: dists are the squared distances of x from n0, n1
            n0, n1 = self._get_nearest_nodes(x)
            Tn0, Tn1 = n0.data.th, n1.data.th

            Xn0 = n0.get_samples()
            Vn0 = n0.get_neighbors()

            # 3) ====> Case 1: The new point is far enough both from the nearest and the second nearest neuron
            if dist_samp2node(x, n0) > Tn0 and dist_samp2node(x, n0) > self.d_min:
                # 3.1) >>>> Create a new neuron in the graph
                node = self.graph.add_node(NGNodeData(x))

                # 3.2) >>>> Update the threshold of the node
                self._update_threshold(node)

                self.graph.add_edge(node, self.graph.add_node(SampleNodeData(x)), SampleEdgeData())

            else:
                # 4) ====> Case2: The new point is close enough to the nearest neuron, but far enough from the second
                #                 nearest neuron
                if dist_samp2node(x, n1) > Tn1 and dist_samp2node(x, n1) > self.d_min:

                    # 4.1) >>>> Create a new neuron in the graph
                    node = self.graph.add_node(NGNodeData(x))
                    # 4.2) >>>> Create a new edge
                    self.graph.add_edge(node, n0, NGEdgeData())

                    self.graph.add_edge(node, self.graph.add_node(SampleNodeData(x)), SampleEdgeData())

                    # 4.3) >>>> Update the threshold of the two nodes
                    self._update_threshold(node), self._update_threshold(n0)

                # 5) ====> Case 3: The new point is close enough to both the nearest and the second nearest neuron.
                #                  It will be assigned to the closed neuron and the age of the emanating edge increased.
                else:

                    # 5.0) >>>> Increase age of the emanating edges
                    for e in n0.get_edges():
                        e.data.age += 1

                    # 5.1) >>>> Compute the current value of eps_b and eps_n and move nearest node and its neighbourhood
                    eps_b = np.sqrt(1. / len(Xn0))
                    eps_n = np.sqrt(1. / (100. * len(Xn0)))

                    # 5.2) >>>> Create a new sample node in the graph and add the new edge
                    self.graph.add_edge(n0, self.graph.add_node(SampleNodeData(x)), SampleEdgeData())

                    # 5.3) >>>> Update the threshold
                    self._update_threshold(n0)

                    # 5.4) >>>> Move the closest node towards x
                    _move_node(n0, x, eps_b)

                    # 5.5) >>>> Neighbors position update
                    for nn in Vn0:
                        _move_node(nn, x, eps_n)

                    # 5.6) >>>> Update n0<->n1 edge
                    if n1 in Vn0:
                        # 5.6.1) >>>> Reset the edge's age to 0
                        (n0.get_edges(neighbor=n1))[0].data.age = 0

                    else:
                        # 5.6.2) >>>> Create a new edge with age 0
                        self.graph.add_edge(n0, n1, NGEdgeData())

                    # 5.7) >>>> Remove old edges
                    # self._remove_old_edges(n0.get_edges())
                    _remove_old_edges()

            # 6) ====> Merging process is optional and can be set to +inf, may be good
            while len(self.graph.neuron_nodes) > self.max_nodes:
                # 6.1) >>>> Execute the merging process with the updated k1
                self._merging(self.k2)
                # 6.2) >>>> Update k1
                self.k2 += self.d

            # 6) ====> Adaptation 1:
            #while sum(np.array(list(map(self.get_nearest_distance, self.graph.neuron_nodes))) < self.d_min) > 1 or len(self.graph.neuron_nodes) > self.max_nodes:
            #    self._merging(k2)
            #    k2 += self.d

        # 7) ====> Adaptation 2: last check on the distance between nodes.
        #while sum(np.array(list(map(self.get_nearest_distance, self.graph.neuron_nodes))) < self.d_min) > 1:
        #    self._merging(self.k2)
        #    self.k2 += self.d

    def _merging(self, k):

        def _remove_old_edges():
            """Remove all edges older than the maximal age."""
            for edge in g_tilde.neuron_edges:
                if edge.data.age > max_age:
                    g_tilde.remove_edge(edge)

        g_tilde = ExtendedGraph()
        migrate2node = lambda n, Xn: [g_tilde.add_edge(n, g_tilde.add_node(ii.data), SampleEdgeData()) for ii in Xn]
        dist2 = lambda _y, _nn: np.linalg.norm(_y.data.pos - _nn.data.pos)
        max_age = self.max_age

        _node1 = self.graph.neuron_nodes.pop(int(np.round(np.random.uniform(0, 1) * len(self.graph.neuron_nodes) - 1)))
        _node2 = self.graph.neuron_nodes.pop(int(np.round(np.random.uniform(0, 1) * len(self.graph.neuron_nodes) - 1)))

        node1 = g_tilde.add_node(_node1.data)
        node2 = g_tilde.add_node(_node2.data)

        g_tilde.add_edge(node1, node2, NGEdgeData())

        migrate2node(node1, _node1.get_samples()), migrate2node(node2, _node2.get_samples())

        self._update_threshold(node1), self._update_threshold(node2)

        for y in self.graph.neuron_nodes:

            (n0_tilde, n1_tilde), _ = _get_nearest_nodes(g_tilde, y)

            Xy = y.get_samples()

            Xn0_tilde = n0_tilde.get_samples()
            Vn0_tilde = n0_tilde.get_neighbors()

            el1 = (len(Xy) * dist2(y, n0_tilde)) / k
            el2 = (len(Xy) * dist2(y, n1_tilde)) / k

            rand = np.random.uniform(0, 1)

            if rand < min(el1, 1):
                node = g_tilde.add_node(y.data)
                migrate2node(node, Xy)
                self._update_threshold(node)

            else:
                if rand < min(el2, 1):
                    node = g_tilde.add_node(y.data)
                    g_tilde.add_edge(node, n0_tilde, NGEdgeData())
                    migrate2node(node, Xy)
                    self._update_threshold(node), self._update_threshold(n0_tilde)

                else:

                    # 5.0) >>>> Increase age of the emanating edges
                    for e in n0_tilde.get_edges():
                        e.data.age += 1

                    # 5.1) >>>> Compute the current value of eps_b and move nearest node and its neighbourhood
                    eps_b = 1. / len(Xn0_tilde)
                    eps_n = 1. / (100. * len(Xn0_tilde))

                    # 5.3) >>>> Migrate old samples to the node.
                    migrate2node(n0_tilde, Xy)

                    # 5.2) >>>> Update threshold
                    self._update_threshold(n0_tilde)

                    # 5.4) >>>> Move the closest node toward the new item
                    _move_node(n0_tilde, y.data.pos, eps_b)
                    #n0_tilde.data.pos = eps_b * (y.data.pos - n0_tilde.data.pos)

                    # 5.5) >>>> Move the neighborhood toward the new item
                    for node in Vn0_tilde:
                        _move_node(node, y.data.pos, eps_n)
                        # node.data.pos = eps_n * (y.data.pos - node.data.pos)

                    # 5.6) >>>> Update the link between n0-n1 edges
                    if n1_tilde in Vn0_tilde:
                        # should be one edge only
                        (n0_tilde.get_edges(neighbor=n1_tilde))[0].data.age = 0

                    else:
                        g_tilde.add_edge(n0_tilde, n1_tilde, NGEdgeData())

                    # 5.7) >>>> Remove old edges from the graph
                    _remove_old_edges()

        del self.graph
        self.graph = g_tilde
        del g_tilde
        return

    def nearest_neighbor(self, input_):
        """Assign each point in the input data to the nearest node in
        the graph. Return the list of the nearest node instances, and
        the list of distances.
        Executing this function will close the training phase if
        necessary."""
        super(AdaptiveIncrementalNeuralGasNode, self).execute(input_)

        nodes_ = []
        dists = []
        for x in input_:
            (n0, _), dist = _get_nearest_nodes(x)
            nodes_.append(n0)
            dists.append(numx.sqrt(dist[0]))
        return nodes_, dists

    def get_nodes_position(self):
        return np.array([n.data.pos for n in self.graph.neuron_nodes], dtype=self.dtype)
