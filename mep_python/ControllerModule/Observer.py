#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import time
import threading
import os
from watchdog.observers import Observer
from ViewModule.Logger import InfoLogger
from ModelModule.PathObserver import PathObserver

thread_start_notify = "[INFO::ObserverManager] Starting thread %s"
thread_stop_notify = "[INFO::ObserverManager] Stopping thread %s"


class PyObserverThread(threading.Thread):
    """
    Observe the given path in order to notify the occurrence of events.
    """

    def __init__(self, thread_id, name, queue, OBSERVED_PATH):
        """
        Constructor of the PyAcquisitionManager thread.
        :param thread_id: the identifier of the thread
        :param name: the name of the thread
        :param queue: the queue of the last uploaded acquisition
        :param OBSERVED_PATH: the path in which all the acquisition are locate
        """
        threading.Thread.__init__(self)
        self.threadID, self.name, self.queue = thread_id, name, queue
        self.stop_flag = False
        self.logger, self.pathOb, self.observer = InfoLogger(), PathObserver(acq_queue=self.queue), Observer()
        # TODO: replace with original
        self.observer.schedule(self.pathOb, path=OBSERVED_PATH, recursive=True)
        # self.observer.schedule(self.pathOb, path=os.getcwd(), recursive=True)


    def run(self):
        """
        The thread keep active the observer on the observed folder.
        :return: None
        """
        global thread_start_notify, thread_stop_notify

        self.logger.write2log(thread_start_notify % self.name)

        # Start the observer
        self.observer.start()

        while not self.stop_flag:
            time.sleep(1)

        # Stop the observer && wait for its stop
        self.observer.stop()
        self.observer.join()

        self.logger.write2log(thread_stop_notify % self.name)
        return

    def set_stop_flag(self):
        """
        Setter of the parameter stop_flag, which is set to 0 when the thread have to stop by user interrupt.
        :return: None
        """
        self.stop_flag = True
        return
