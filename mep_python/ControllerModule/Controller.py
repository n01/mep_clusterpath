#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import os
import queue
import threading
import configparser

from time import time, sleep
from ModelModule.DatabaseManager import *
from ViewModule.Logger import InfoLogger
from ControllerModule.LoadWorker import LoadWorker
from ControllerModule.Observer import PyObserverThread
from ControllerModule.Cluster import PyClusterController
from ControllerModule.Pyclustering import PyclusteringThread

config = configparser.ConfigParser()
config.read('config.txt')
BUF_SIZE = config['Devel_Info'].getint('BUF_SIZE')
max_thread_num = config['Devel_Info'].getint('max_thread_num')
OBSERVED_PATH = config['Paths']['observed_path']
ety_db = config['Devel_Info'].getboolean('ety_db')

class Controller(object):

    def __init__(self):
        """
        Constructor of the Controller class
        """
        global BUF_SIZE, OBSERVED_PATH

        def get_init_queue(_queue):
            for root, dirs, files in os.walk(OBSERVED_PATH):
                for name in files:
                    if name.endswith('.zip') and not mep_folder_check(root, name) and not('not_defined' in root):
                        _queue.put(item=os.path.join(root, name))

        self.threads, self.queue, self.logger = [], queue.LifoQueue(BUF_SIZE), InfoLogger()
        self.lock = threading.Lock()

        self.py_observer_thread = PyObserverThread(1, "ObserverManager", self.queue, OBSERVED_PATH)
        self.py_cluster_thread = PyClusterController(2, "ClusterManager")
        # Making our pycluster thread
        self.py_pycluster_thread = PyclusteringThread(3, "PyclusterManager")

        """
        The mep-db needs to be filled with the file already into the observed folder. When the script is launched it
        checks the whole content of the folder and proceed to upload it.
        """
        if ety_db:
            get_init_queue(self.queue)

    def main_loop(self):
        """
        Main_loop method of the Controller class. The method iterate until a KeyboardInterrupt event occur.
        :return: None
        """
        global max_thread_num

        def _thread_counter():
            return sum([ii.is_alive() for ii in th])

        obs_th, clu_th, pyclu_th, acq_queue, th = self.py_observer_thread, self.py_cluster_thread, self.py_pycluster_thread, self.queue, self.threads

        obs_th.start()
        clu_th.start()
        # Start our thread
        pyclu_th.start()

        try:
            while True:
                if acq_queue.qsize() > 0 and max_thread_num - _thread_counter() > 0:
                    for tt in range(0, min(min(acq_queue.qsize(), 3), max_thread_num - _thread_counter())):
                        item = acq_queue.get()
                        ts = int(time()*1000)
                        worker = LoadWorker(ts, "Worker%s" % ts, item, self.lock)
                        worker.start()
                        th.append(worker)
                        sleep(0.2)
                else:
                    sleep(0.2)                                                  # Time lapse between one check of the
                                                                                # queue and the next one

        except KeyboardInterrupt:

            obs_th.stop_flag = True                                             # Exit flag to 0 in order to stop
            obs_th.join()                                                       # Wait for the obeserver thread to stop
            clu_th.stop_flag = True                                             # Exit flag to 0 in order to stop
            clu_th.join()
            # Stop our thread
            pyclu_th.stop_flag = True
            pyclu_th.join()                                                       # Wait for the cluster thread to stop

            [ii.join() for ii in th]                                            # Wait for the worker thread to stop
