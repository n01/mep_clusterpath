#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import threading
from time import sleep
from datetime import datetime
from ModelModule.DatabaseManager import *
from ModelModule.AingManager import *
from ViewModule.Logger import InfoLogger

thread_start_notify = "[INFO::ClusterManager] Starting thread %s"
thread_stop_notify = "[INFO::ClusterManager] Stopping thread %s"
set_info = "[INFO::ClusterManager] Retrieved points number: %s ====> Max nodes number: %s"
plt_path = "./plt/%s"


class PyClusterController(threading.Thread):
    """
    The cluster management class which effectively operate the clustering operation.
    """
    def __init__(self, thread_id, name):
        """
        Constructor of the PyClusterManager
        :param thread_id: the identifier of the current thread
        :param name: the name of the current thread
        """
        threading.Thread.__init__(self)
        self.thread_id, self.name, self.stop_flag = thread_id, name, False
        self.logger = InfoLogger()
        self.queue = get_clusterpath_queue(2000)

    def run(self):
        """
        Main loop of the PyClusterManager overriding of the run method.
        :return: None
        """
        self.logger.write2log(thread_start_notify % self.name)

        while not self.stop_flag:
            if len(self.queue) > 0:
                item = self.queue.pop()
                clean_neuron_table(item)
                print(item)
                start = datetime.now()
                msg = Clusterpath(item, shuffle=False)
                end = datetime.now()
                rob = end - start
                print(rob)
                pop_clusterpath_queue(item)
                self.logger.write2log(msg)
            else:
                sleep(1)

            self.queue = get_clusterpath_queue(2000)

        self.logger.write2log(thread_stop_notify % self.name)
