#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import os
import zipfile
import threading
from time import sleep, strftime
from ModelModule.DatabaseManager import *
from ViewModule.Logger import InfoLogger


target = ['MEP_DEVICE_INFO.txt', 'MEP_MOTION_INFO.txt', 'MEP_NMEA_INFO.txt', 'MEP_POSITION_INFO.txt', 'PoseSE3(W).log',
          'TPoseSE(W)']

thread_start_notify = "[INFO::LoaderWorker] Starting thread %s"
thread_stop_notify = "[INFO::LoaderWorker] Stopping thread %s"
new_zip_found_notify = "[INFO::LoaderWorker] New zip found at %s"
file_not_found = '[ERROR::LoaderWorker] File Not Found %s ====> %s'
bad_zip_file = '[ERROR::LoaderWorker] Bad Zip File (Extraction Failed) %s ====> %s'


class LoadWorker(threading.Thread):
    """
    PyLoaderManager it's a class extending threading class. Its aim is to parallelize the processing of the loading
    stage of the new uploaded path.
    """

    def __init__(self, thread_id, name, item, lock):
        """
        Constructor of the PyLoaderManager.
        :param thread_id: the identifier of the current thread
        :param name: the name of the current thread
        :param item: the current
        """
        threading.Thread.__init__(self)

        self.thread_id, self.name, self.item, self.lock = thread_id, name, item, lock
        self.logger = InfoLogger

    def run(self):
        """
        Main loop of the PyLoaderManager overriding of the run method.
        :return: None
        """
        path = lambda i: os.path.join(zip_item[0:-4], target[i])
        extract = lambda i: zip_item.split('/')[i]

        msg = []
        zip_item, th_name = self.item, self.name
        root_folder = os.path.split(zip_item)[-2]
        zip_name = extract(-1)
        pthNMEA, pthMEPposition, pthPoseSE3 = path(2), path(3), path(5)

        self.logger.write2log(thread_start_notify % th_name)
        self.logger.write2log(new_zip_found_notify % zip_item)

        # a) ====> Wait for the complete file copy, each time control the zip's size, if no changes then proceed.
        copying, size2 = True, -1
        while copying:
            size = os.path.getsize(zip_item)
            if size == size2:
                copying = False
            else:
                size2 = os.path.getsize(zip_item)
                sleep(2)

        # b) ====> When a new zip file is found, it is extracted and the content elaborated.
        if zip_name.endswith('.zip') and not (zip_item.find('not_defined') != -1) and zipfile.is_zipfile(zip_item):

            # b1) >>>> Declare the current zipfile and the new path that should be created
            zip_file, new_path = zipfile.ZipFile(zip_item), os.path.join(root_folder, zip_name[0:-4])

            # b2) >>>> A new folder is created iff it does not exists
            if not os.path.exists(new_path):
                os.mkdir(new_path)

            # b3) >>>> Extract the content of the zipfile into the new folder
            try:
                zip_file.extractall(new_path)
                zip_file.close()
                del zip_file
            # b4) >>>> Exception generated if the zip is a bad zip
            except zipfile.BadZipFile:
                return msg.append(bad_zip_file % (zip_name, root_folder))

        # c) ====> GNSS Dica correction, load informations from the OSM database
        # Clean the database from previous element in order to keep it reactive.
        clean_osm_table()

        if os.path.isfile(pthNMEA):
            load_osm(pthNMEA)

        # @TODO remove this part
        # ------------------------------- THIS IS TEMPORARY ------------------------------------------------------------
        # if os.path.isfile(pthMEPposition):
            # load_pose_tmp(pthMEPposition)
        # --------------------------------------------------------------------------------------------------------------

        # d) ====> Call the fusion handler data processing
        #fusion_handler(zip_item[0:-4])

        # e) ====> Call the model function to load the PoseSE3 files into the database
        #if os.path.isfile(pthPoseSE3):
        #    [msg.append(jj) for jj in load_pose(pthPoseSE3)]
            # Unroll the messages turned back by the method execution
        #else:
        #    msg.append(file_not_found % (TgtFile[4], pthPoseSE3))
            # Only if the file does not exists

        # f) ====> Call the model function to load the MEP_POSITION_INFO.txt into the database
        if os.path.isfile(pthMEPposition):
            # f1) >>>> Unroll the messages turned back by the method execution
            [msg.append(jj) for jj in load_gps(pthMEPposition)]
        else:
            # f2) >>>> Only if the file does not exists
            msg.append(file_not_found % (target[3], pthMEPposition))

        # g) ====> Update the list of folders elaborated by mep-clusterpath.
        insert_folder(extract(-3), extract(-1), extract(-2), extract(-4))

        # e) ====> Update the mep-clusterpath queue.
        push_clusterpath_queue(extract(-2))

        msg.append(thread_stop_notify % th_name)
        self.logger.write2log(msg)

        return
