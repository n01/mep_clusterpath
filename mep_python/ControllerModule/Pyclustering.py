'''
Here is a main thread for clustering data with algorithms given
'''

import threading
from time import sleep
from datetime import datetime

from ModelModule.DatabaseManager import *
from ModelModule.AingManager import *
from ViewModule.Logger import InfoLogger

from pyclustering.cluster.kmeans import kmeans
from pyclustering.cluster.optics import optics
from pyclustering.cluster.dbscan import dbscan

thread_start_notify = "[INFO::Pyclustering] Starting thread %s"
thread_stop_notify = "[INFO::Pyclustering] Stopping thread %s"

class PyclusteringThread(threading.Thread):
    # This is the constructor of the clustering class
    def __init__(self, thread_id, name):
        # Make the thread
        threading.Thread.__init__(self)
        self.thread_id, self.name, self.stop_flag = thread_id, name, False
        self.logger = InfoLogger()
        # This line will get the limit data stored in limit table to decide from where it should
        # continue clustering
        self.limit = get_limit_data()
        self.res = []
        self.data = []

    # Main loop of the thread
    def run(self):
        self.logger.write2log(thread_start_notify % self.name)
        # A loop that Check some keyboard interrupt occured to stop thread
        while not self.stop_flag:
            # This loop checks that there is enough data on table to read
            while len(self.res) != 1000 and not self.stop_flag:
                sleep(0.2)
                # Read data with a fixed window (1000)
                self.res = get_limit_gps_position(self.limit, self.limit+1000)
                print('limit: ', self.limit, ' len: ', len(self.res))

            xdata = []
            ydata = []
            temp = dict()
            # Preparing data
            for i in self.res:
                temp = i[5], i[6]
                self.data.append(temp)
                xdata.clear()
                xdata.append(i[5])
                xdata.append(i[6])
                ydata.append(xdata)

            self.res.clear()

            # Running algorithms
            self.kmeans_func()
            self.optics_func()
            self.dbscan_func()

            # Correct variables
            self.limit = self.limit + 1000
            self.data.clear()
            ydata.clear()
            xdata.clear()

        # Insert limit number in limit table
        insert_limit_data(self.limit)
        self.logger.write2log(thread_stop_notify % self.name)


    ####################### K-Means Algorithm #############################
    def kmeans_func(self):
        start_centers = [ [45.8386509747745, 9.06182105319716], [45.8386094288896, 9.06191748978146] ]
        kmeans_instance = kmeans(self.data, start_centers)
        kmeans_instance.process()
        clusters = kmeans_instance.get_clusters()
        centers = kmeans_instance.get_centers()

        # Insert clustered data in kmeans table
        insert_clustered_data('kmeans', clusters, self.limit)

    ####################### DB-Scan Algorithm ###########################
    def dbscan_func(self):
        dbscan_instance = dbscan(self.data, 2, 3)
        dbscan_instance.process()
        clusters = dbscan_instance.get_clusters()

        # Insert clustered data in dbscan table
        insert_clustered_data('dbscan', clusters, self.limit)

    ###################### Optics Algorithm ###################
    def optics_func(self):
        optics_instance = optics(self.data, 2, 3)
        optics_instance.process()
        clusters = optics_instance.get_clusters()

        # Insert clustered data in optics table
        insert_clustered_data('optics', clusters, self.limit)
