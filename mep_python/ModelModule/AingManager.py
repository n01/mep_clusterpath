#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

# import os
# import time
# import pickle
import numpy as np
import pandas as pd
from scipy import stats
from AING.AdaptiveIncrementalNeuralGasNode import AdaptiveIncrementalNeuralGasNode
from ModelModule.DatabaseManager import get_city_item, load_neuron

set_info = "[INFO::ClusterManager] Retrieved points number: %s ====> Max nodes number: %s"
# plt_path = "./plt/%s"


def Clusterpath(loc, shuffle=True):
    """
    Adaptive Incremental Neural Gas network function Manager.
    :param shuffle: shuffle the data set if True, ``un-shuffle'' otherwise
    :param loc: the current clustered city
    :return: None
    """
    msg = []
    step = 0.25
    mapping = range(1, 10)

    def raw_composer(node):
        samples = np.array(list(map(lambda item: list(node.data.pos), node.get_samples())))

        mu, sigmall, slope = np.array([0, 0]), np.array([[0, 0], [0, 0]]), 0

        if len(samples) > 1:
            # b) Mean of the latitude and longitude vectors.
            mu = np.mean(samples, axis=0)

            # c) Covariance matrix of the latitude and longitude vectors.
            sigmall = np.cov(np.array(samples).T)

            slope, intercept, r_value, p_value, std_err = stats.linregress(samples[1, :], samples[0, :])

        # d) Build and append the data structure.
        lat, long = node.data.pos

        # e) Compose the raw
        load_neuron([lat, long, float(mu[0]), float(mu[1]), float(sigmall[0, 0]), float(sigmall[1, 1]), np.arctan(slope)], samples, loc)
        return

    # 1) ===> Retrive the point of the current city and compose the data set
    df = get_city_item(loc)
    df = pd.DataFrame(df, columns=mapping)

    # 2) ===> Shuffle the data set
    if shuffle:
        df = df.sample(frac=1).reset_index(drop=True)

    # 3) ===> Extract the columns containing latitude and longitude
    x_, y_ = np.array(df.iloc[:, 5]).reshape(1, df.shape[0]), np.array(df.iloc[:, 6]).reshape(1, df.shape[0])
    input_ = (np.concatenate([x_, y_]).reshape(2, df.shape[0])).T
    # input_ = input_.transpose()

    # 4) ===> Build the initial point vector.
    n0 = [np.array(df.iloc[0, 5]), np.array(df.iloc[0, 6])]
    n1 = [np.array(df.iloc[1, 5]), np.array(df.iloc[1, 6])]
    init_n = np.array([n0, n1])
    # 4) ===> Pop the first 2 element of the input vector
    # init_n = np.array([df.pop(0), df.pop(1)])

    # 5) ===> Create the Adaptive Incremental Neural Gas Node Object.
    max_neurons = 500
    msg.append(set_info % (df.shape[0], max_neurons))
    # MEP-Clusterpath
    # aing = AdaptiveIncrementalNeuralGasNode(init=init_n, d = 3.0e-6, d_min=2.0e-5, max_nodes=max_neurons) # This works
    # aing = AdaptiveIncrementalNeuralGasNode(init=init_n, d=5e-6, 5d_min=3e-5, max_nodes=max_neurons)
    # aing = AdaptiveIncrementalNeuralGasNode(init=init_n, d=5e-5, d_min=5e-5, max_nodes=max_neurons)  # This works final!!!!

    aing = AdaptiveIncrementalNeuralGasNode(init=init_n, d=5e-6, d_min=5e-5, max_nodes=max_neurons, max_age=100)  # For test purpose only

    # AING
    # aing = AdaptiveIncrementalNeuralGasNode(init=init_n, d=5e-6, d_min=5e-5, max_nodes=max_neurons, max_age=50)  # For test purpose only

    # 8) ===> Train the algorithm.
    step_num = round(step * df.shape[0])

    for i in range(0, df.shape[0], step_num):
       # aing.train(input_[i: min(i+int(step), df.shape[0])])
       aing.train(input_[i: min(i+step_num, df.shape[0])])

    # 9) ===> Stop training.
    aing.stop_training()

    # 10) ===> Get the clusters.
    objs = aing.graph.connected_components()

    # neurons = aing.graph.neuron_nodes

    # map(raw_composer, neurons)

    # return msg

    # 11) ===> Plot on file all visual results.
    #if not os.path.exists(plt_path % loc):
    #    os.mkdir(plt_path % loc)

    # time_zone = time.strftime('%Y-%m-%d %H:%M:%S')

    # Neurons ===> Struct [[coord, mu, sigma], [coord, mu, sigma], ...] => coord:[1x2] | mu:[1x2] | sigma:[2x2]}
    # neurons = []

    # 12) ===> Retrieve and transform all the relevant data.
    for j, cltr in enumerate(objs):
        for node in cltr:

            # a) Extract the related samples from each neuron.
            samples = list(map(lambda item: list(item.data.pos), node.get_samples()))

            mu, sigmall, slope = np.array([0, 0]), np.array([[0, 0], [0, 0]]), 0

            if len(samples) > 1:

                # b) Mean of the latitude and longitude vectors.
                mu = np.mean(samples, axis=0)

                # c) Covariance matrix of the latitude and longitude vectors.
                sigmall = np.cov(np.array(samples).T)

                slope, intercept, r_value, p_value, std_err = stats.linregress(np.array(samples)[:, 1].T, np.array(samples)[:, 0].T)


            # d) Build and append the data structure.
            lat, long = node.data.pos
            ds = [lat, long, float(mu[0]), float(mu[1]), float(sigmall[0, 0]), float(sigmall[1, 1]), float(slope)]
            # neurons.append([lat, long, mu[0], mu[1], sigmall[0, 0], sigmall[1, 1], sigmall[0, 1]])
            # e) Load the neurons and its related samples into the database.
            load_neuron(ds, samples, loc)

    # Save the data structure for the visualization purpose only through the MEP Heatmap visualizer.
    #with open(os.path.join(plt_path % loc, time_zone), 'wb') as f:
    #    pickle.dump(neurons, f)

    return msg
