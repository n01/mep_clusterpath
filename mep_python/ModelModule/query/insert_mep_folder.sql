insert into public.mep_folders_table(device_id, item_name, location, user_email, elaboration_dt, elaboration_flag)
values(%s, %s, %s, %s, %s, %s)
on CONFLICT (device_id, item_name) do update set elaboration_flag = true;