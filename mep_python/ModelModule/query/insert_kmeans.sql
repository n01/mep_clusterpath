insert into pycluster.kmeans (device_id, ts, latitude, longitude, tr_point, accessibility_level)
values ( %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326), %s)
on CONFLICT (device_id, ts) do
  update set tr_point = ST_SetSRID(ST_MakePoint(pycluster.kmeans.longitude, pycluster.kmeans.latitude), 4326), accessibility_level = 1;
