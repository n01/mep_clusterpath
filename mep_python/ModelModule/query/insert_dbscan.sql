insert into pycluster.dbscan (device_id, ts, latitude, longitude, tr_point, accessibility_level)
values ( %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326), %s)
on CONFLICT (device_id, ts) do
  update set tr_point = ST_SetSRID(ST_MakePoint(pycluster.dbscan.longitude, pycluster.dbscan.latitude), 4326), accessibility_level = 1;
