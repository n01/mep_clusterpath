insert into public.type_gps_position_point (device_id, ts, user_email, time_zone, location, latitude, longitude, tr_point, accessibility_level)
values ( %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326), %s)
on CONFLICT (device_id, ts) do
  update set tr_point = ST_SetSRID(ST_MakePoint(public.type_gps_position_point.longitude, public.type_gps_position_point.latitude), 4326), accessibility_level = 1;
