insert into public.neuron_pt(ts, time_zone, location, latitude, longitude, coord, mu_x, mu_y, sigma_x, sigma_y, theta,pts_list,  pts_num, accessibility_level)
values(round(EXTRACT(EPOCH FROM now())*1000), now(), %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326), %s, %s, %s, %s, %s, %s, %s, %s);
