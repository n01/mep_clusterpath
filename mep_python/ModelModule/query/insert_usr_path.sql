INSERT INTO public.user_path(device_id, user_email, time_zone, path)
VALUES(%s, %s, %s, (SELECT ST_MakeLine(tr_point)
                      FROM public.type_gps_position_point
                      WHERE device_id = %s AND time_zone = %s));