delete from building.lines;
delete from building.multilinestrings;
delete from building.multipolygons;
delete from building.other_relations;
delete from building.points;

delete from highway.lines;
delete from highway.multilinestrings;
delete from highway.multipolygons;
delete from highway.other_relations;
delete from highway.points;

delete from public.building_area;
delete from public.building_perimeter;
delete from public.building_segment;
delete from public.highway_segment;
