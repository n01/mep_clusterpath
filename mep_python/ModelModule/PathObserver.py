#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import os
from watchdog.events import FileSystemEventHandler

event_occurred = '[INFO::PathObserver] event %s ====> %s'


class PathObserver(FileSystemEventHandler):
    """
    Class PathHandler define the actions to perform when a new acquisition is added to the MEP_Traces path
    """

    def __init__(self, acq_queue):
        """
        Constructor of the PathObserver
        :param acq_queue: the queue of the last uploaded acquisition
        """
        self.acq_queue = acq_queue
        self.target = '.zip'

    def on_created(self, event):
        """
        React to a new folder creation into the ./trace path by adding its path into the queue
        :param event: the new occurred event
        :return: None
        """
        def get_ext(filename):
            """
            Get the extension of the file.
            :param filename: the name of the file which has to be retrieved the extension
            :return: the extension of the file
            """
            return os.path.splitext(filename)[-1].lower()

        # a) ====> Neglect directories
        if event.is_directory:
            return

        # b) ====> Only zip events are recorded
        if get_ext(event.src_path) == '.zip':
            cur_path = str(event.src_path)
            if not('not_defined' in cur_path):
                # b1) >>>> Update the queue with the new element
                self.acq_queue.put(item=cur_path)
            return

    def on_modified(self, event):
        return

    def on_deleted(self, event):
        return

    @staticmethod
    def get_ext(filename):
        """
        Get the extension of the file.
        :param filename: the name of the file which has to be retrieved the extension
        :return: the extension of the file
        """
        return os.path.splitext(filename)[-1].lower()

    @staticmethod
    def process(self, cur_path):
        return


