#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import os
import sys
import math
import psycopg2
import subprocess
import configparser
import pandas as pd
from time import time, strftime, sleep

# ########################### Information ##############################################################################
db_config = lambda ii: config['DbConnectionConfigurations'][ii]
get_qy = lambda file: open(file, 'r').read()

config = configparser.ConfigParser()
config.read('config.txt')

dbname, user, psw = db_config('dbname'), db_config('user'), db_config('psw')
db_connection_info = "dbname=%s host=localhost user=%s password=%s" % (dbname, user, psw)

tab_nm = 'pose_pt'

target = ['MEP_DEVICE_INFO.txt', 'MEP_MOTION_INFO.txt', 'MEP_NMEA_INFO.txt', 'MEP_POSITION_INFO.txt', 'PoseSE3(W).log',
          'TPoseSE(W)']


# ########################### Query Definition #########################################################################
insert_gps_qy = get_qy('ModelModule/query/insert_gps_pt.sql')
insert_pose_qy = get_qy('ModelModule/query/insert_pose_point.sql')
insert_user_path_qy = get_qy('ModelModule/query/insert_usr_path.sql')
insert_neuron_qy = get_qy('ModelModule/query/insert_neuron_point.sql')
get_city_list_qy = get_qy('ModelModule/query/get_city_list.sql')
get_city_item_qy = get_qy('ModelModule/query/get_city_item.sql')
delete_osm_data_qy = get_qy('ModelModule/query/clean_osm_tables.sql')
delete_aing_data_qy = get_qy('ModelModule/query/clean_cluster_table.sql')
get_city_item_number_qy = get_qy('ModelModule/query/get_city_item_number.sql')
update_mep_folder_list_qy = get_qy('ModelModule/query/insert_mep_folder.sql')
mep_folder_check_qy = get_qy('ModelModule/query/check_mep_folder.sql')
get_clusterpath_queue_qy = get_qy('ModelModule/query/get_clusterpath_queue.sql')
push_aing_queue_qy = get_qy('ModelModule/query/push_aing_queue.sql')
pop_aing_queue_qy = get_qy('ModelModule/query/pop_aing_queue.sql')
get_gps_position_qy = get_qy('ModelModule/query/get_gps_position.sql')
get_limit_gps_position_qy = get_qy('ModelModule/query/get_limit_gps_position.sql')
insert_limit_qy = get_qy('ModelModule/query/insert_limit.sql')
get_limit_qy = get_qy('ModelModule/query/get_limit.sql')
insert_kmeans_qy = get_qy('ModelModule/query/insert_kmeans.sql')
insert_dbscan_qy = get_qy('ModelModule/query/insert_dbscan.sql')
insert_optics_qy = get_qy('ModelModule/query/insert_optics.sql')

# ########################### Messages #################################################################################
loading_error_message = '[ERROR::DbManager] Failed to load %s ====> %s'
loading_init_message = '[DB_EVENT::DbManager] Loading %s ====> %s'
loading_complete_message = '[DB_EVENT::DbManager] Completed %s ====> %s'
loading_neuron_message = '[DB_EVENT::DbManager] Loading %s clustered points'
acquisition_length_info = '[INFO::DbManager] Acquisition length = %s h, %s m'

conn = psycopg2.connect(db_connection_info)
# cur = conn.cursor()


# @TODO revert to the factory method which automatically uploads elements in input.
def load_factory(element):
    global target
    fname = element.split("/")

    # if element == MEP_DEVICE_INFO
    if fname == target[0]:
        msg = None
        return msg
    # if element == MEP_MOTION_INFO
    elif fname == target[1]:
        msg = 'das'
    # if element == MEP_NMEA_INFO
    elif fname == target[2]:
        msg = load_osm(element)
    # if element == MEP_POSITION_INFO
    elif fname == target[3]:
        msg = load_gps(element)
    # if element == PoseSE3(W)
    elif fname == target[4]:
        msg = load_pose()
    # if element == TPoseSE3(W)
    elif fname == target[5]:
        msg = 'asd'
    else:
        raise ValueError('The file is not into the target list!')
    return msg


def load_db_item(element):
    msg = None
    try:
        msg = load_factory(element)
    except ValueError as ve:
        print(ve)
    return msg


# ========================================LOAD INTO THE DATABASE================================================== #
def load_gps(gps_pth):
    """
    Load the current analyzed gps_position path into the postgis database.
    :param gps_pth: the current MEP_POSITION_INFO.txt path to be loaded
    :return: the message containing all the operations
    """
    msg = [loading_init_message % (target[3], gps_pth)]
    mapping = ['timestamp', 'time_zone', 'type', 'x', 'y', 'pnr', 'rec', 'snr']
    df = pd.read_csv(gps_pth, names=mapping)

    try:
        gps_position = df.copy()

        try:  # Extract 'TYPE_GPS_POSITION'
            gps_position = gps_position[gps_position['type'] == 'TYPE_GPS_POSITION']
            gps_position.drop(mapping[5], axis=1, inplace=True)
            gps_position.drop(mapping[6], axis=1, inplace=True)
            gps_position.drop(mapping[7], axis=1, inplace=True)
            gps_position.rename(columns={'x': 'latitude', 'y': 'longitude'}, inplace=True)

        except ImportError:
            msg.append(loading_error_message % (target[3], gps_pth))

        second_pos = (float(gps_position.iloc[-1:, 0] - gps_position.iloc[0, 0]) / 1000)
        hour_pos = math.floor(second_pos / 3600)
        minute_pos = math.floor((second_pos / 3600 - hour_pos) * 60)

        msg.append(acquisition_length_info % (str(hour_pos), str(minute_pos)))

        cur = conn.cursor()

        # Extract user_account, device_id and location
        path_el = gps_pth.split("/")
        usr_account, device_id, location = path_el[-5], path_el[-4], path_el[-3]

        for i in range(0, gps_position.shape[0] - 1):
            timestamp, time_zone = int(gps_position.iloc[i, 0]), gps_position.iloc[i, 1]
            latitude, longitude, accessibility = gps_position.iloc[i, 3], gps_position.iloc[i, 4], 0

            # TODO: commented for now and ignored error
            cur.execute(insert_gps_qy, [device_id, timestamp, usr_account, time_zone, location, latitude, longitude, longitude, latitude, accessibility])
            conn.commit()

        cur.close()

        msg.append(loading_complete_message % (target[3], gps_pth))
    except IndexError:
        msg.append(loading_error_message % (target[3], gps_pth))
    return msg


def load_pose(pose_pth):
    """

    :param pose_pth:
    :return:
    """

    msg = [loading_init_message % (target[4], pose_pth)]

    mapping = [1, 2, 3, 4]
    PoseSE3 = pd.read_csv(pose_pth, names=mapping)  # add the mapping here
    PoseSE3 = PoseSE3.drop_duplicates([1], keep='last').copy()

    # Extract row number, user_account, device_id, location, default accessibility level
    rowPoseSE3, usr, device_id, loc, acy = \
        PoseSE3.shape[0], pose_pth.split("/")[-5], pose_pth.split("/")[-4], pose_pth.split("/")[-3], 0
    cur = conn.cursor()

    for ii in range(1, rowPoseSE3):
        [ts, lat, long, h] = PoseSE3.iloc[ii, 0:3]
        # lat, long, h = PoseSE3.iloc[ii, 1], PoseSE3.iloc[ii, 2], PoseSE3.iloc[ii, 3]

        try:
            cur.execute(insert_pose_qy, [device_id, ts, usr, loc, lat, long, h, long, lat, h, acy])
            conn.commit()  # DB changes made permanent
        except IndexError:
            msg.append(loading_error_message % (target[4], pose_pth))
            return msg

    cur.close()
    msg.append(loading_complete_message % (target[4], pose_pth))
    return msg


def load_osm(nmea_pth):
    msg = [loading_init_message % (target[2], nmea_pth)]

    if not os.path.exists('./mep-osm-file'):
        os.mkdir('./mep-osm-file')
    # Build the command for the bash script
    cmd = ['./mep-osm/update_db_pt', '%s' % dbname, '%s' % tab_nm, '%s' % nmea_pth, 'all', '0', '1']
    # Launch the command to load osm data
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    # Wait for the script termination
    p.communicate()
    return msg


def load_neuron(ds, samples, loc):
    msg = [loading_neuron_message % loc]
    cur = conn.cursor()
    # Data record:::: <<ts, time_zone, location, latitude, longitude, coordinate ,mu_x , mu_y, sigma_x, sigma_y,
    # pts_list, pts_num, ac_lev>>
    cur.execute(insert_neuron_qy, [loc, ds[0], ds[1], ds[1], ds[0], ds[2], ds[3], ds[4], ds[5], ds[6], samples, len(samples), 1])
    conn.commit()
    cur.close()
    return msg


# @TODO once Bardaro's algorithm will be operative this could be removed.
def load_pose_tmp(tmp_pth):
    """
    Load the current analyzed gps_position path into the postgis database.
    :param tmp_pth: the current MEP_POSITION_INFO.txt path to be loaded
    :return: the message containing all the operations
    """
    insPositionQy_tmp = ("INSERT INTO public.pose_pt \n"
                         "(device_id, ts, user_email, time_zone, \n"
                         "location, latitude, longitude, coord, accessibility_level) \n"
                         "VALUES ( %s, %s, %s, %s, %s, %s, %s, ST_SetSRID(ST_MakePoint(%s, %s), 4326), %s);")

    msg, mapping = [loading_init_message % (target[3], tmp_pth)], \
                   ['timestamp', 'time_zone', 'type', 'x', 'y', 'pnr', 'rec', 'snr']

    df = pd.read_csv(tmp_pth, names=mapping)

    try:
        gps_position = df.copy()  # Copy the data frame

        try:  # Extract 'TYPE_GPS_POSITION'
            gps_position = gps_position[gps_position['type'] == 'TYPE_GPS_POSITION']
            gps_position.drop(mapping[5], axis=1, inplace=True)
            gps_position.drop(mapping[6], axis=1, inplace=True)
            gps_position.drop(mapping[7], axis=1, inplace=True)
            gps_position.rename(columns={'x': 'latitude', 'y': 'longitude'}, inplace=True)

        except ImportError:
            msg.append(loading_error_message % (target[3], tmp_pth))

        second_pos = (float(gps_position.iloc[-1:, 0] - gps_position.iloc[0, 0]) / 1000)
        hour_pos = math.floor(second_pos / 3600)
        minute_pos = math.floor((second_pos / 3600 - hour_pos) * 60)

        msg.append(acquisition_length_info % (str(hour_pos), str(minute_pos)))

        cur = conn.cursor()

        # Extract user_account, device_id and location
        path_el = tmp_pth.split("/")
        usr_email, d_id, loc = path_el[-5], path_el[-4], path_el[-3]

        for i in range(0, gps_position.shape[0] - 1):
            # Serialized timestamp, Timestamp, Latitude, Longitude, Accessibility level set = 0
            timestamp, time_zone = int(gps_position.iloc[i, 0]), gps_position.iloc[i, 1]
            latitude, longitude, acc = gps_position.iloc[i, 3], gps_position.iloc[i, 4], 0

            qy_item = [d_id, timestamp, usr_email, time_zone, loc, latitude, longitude, longitude, latitude, acc]
            cur.execute(insPositionQy_tmp, qy_item)
            conn.commit()  # DB changes made permanent

        cur.close()
        msg.append(loading_complete_message % (target[3], tmp_pth))
    except IndexError:
        msg.append(loading_error_message % (target[3], tmp_pth))
    return msg


def insert_folder(dev_id, fold_name, loc, email, date_time=strftime('%Y-%m-%d'), e_flag='True'):
    cur = conn.cursor()
    cur.execute(update_mep_folder_list_qy, [dev_id, fold_name, loc, email, date_time, e_flag])
    conn.commit()
    cur.close()
    return


def push_clusterpath_queue(loc):
    cur = conn.cursor()
    cur.execute(push_aing_queue_qy, [loc])
    conn.commit()
    cur.close()
    return


def pop_clusterpath_queue(loc):
    cur = conn.cursor()
    cur.execute(pop_aing_queue_qy, [loc])
    conn.commit()
    cur.close()
    return


# ========================================DATABASE OPERATIONS===================================================== #
def clean_osm_table():
    cur = conn.cursor()
    # cur.execute(delete_osm_data_qy)
    conn.commit()
    cur.close()
    return


def clean_neuron_table(loc):
    cur = conn.cursor()
    # cur.execute(delete_aing_data_qy, [loc])
    conn.commit()
    cur.close()
    return


def mep_folder_check(root, name):
    device_id = root.split('/')[-2]
    cur = conn.cursor()
    cur.execute(mep_folder_check_qy, [device_id, name])
    res = bool(cur.fetchone()[0])
    cur.close()
    return res


# ========================================RETRIEVE FROM THE DATABASE============================================== #
def get_db_info():
    """
    Retrieve some information about the Python, Pandas and Postgis version.
    :return: None
    """
    cur = conn.cursor()
    cur.execute('SELECT * FROM Postgis_lib_version();')
    c = cur.fetchall()
    cur.close()
    py_version, pandas_version = str('Python version ' + sys.version), str('Pandas version ' + pd.__version__)
    pg_lib_version = str('Postgis_lib_version: ' + str(c))
    res = [str(py_version), str(pandas_version), str(pg_lib_version)]
    return res


def get_city_list():
    """

    :return:
    """
    cur = conn.cursor()
    cur.execute(get_city_list_qy)
    res = cur.fetchall()
    res = list((res[i][0] for i in range(len(res))))
    cur.close()
    return res


def get_city_item(name):
    """
    Retrieve all the path points within a city.
    :param name: the location over which perform the analysis
    :return: the whole data set of points acquired by the users within a city
    """
    cur = conn.cursor()
    cur.execute(get_city_item_qy, [name])
    res = cur.fetchall()
    cur.close()
    return res


def get_city_item_number(name):
    """

    :param name:
    :return:
    """
    cur = conn.cursor()
    cur.execute(get_city_item_number_qy, [name])
    res = cur.fetchall()[0][0]
    cur.close()
    return res


def get_clusterpath_queue(min_num):
    """
    Retrieve the list of the clustered locations.
    :return:
    """
    cur = conn.cursor()
    cur.execute(get_clusterpath_queue_qy, [min_num])
    res = cur.fetchall()
    res = list((res[i][0] for i in range(len(res))))
    cur.close()
    return res

# This function has no effect
def get_gps_position():
    cur = conn.cursor()
    cur.execute(get_gps_position_qy)
    res = []
    res = cur.fetchmany(500)
    cur.close()
    return res


# This function reads data in a window start from min to max variables given
def get_limit_gps_position(min, max):
    cur = conn.cursor()
    cur.execute(get_limit_gps_position_qy, [min, max])
    res = []
    res = cur.fetchall()
    cur.close()
    return res


# This function inserts data according to name of the algorithm given to the right table
def insert_clustered_data(algorithm, clusters, min):
    cur = conn.cursor()
    # Read the data in a windows given
    data = get_limit_gps_position(min, min+1000)
    w = 0
    for i in range(len(clusters)):
        if len(clusters[i]) > w:
            w = len(clusters[i])

    h = len(clusters)

    # Making an output array
    output = [[0] * w] * h
    # This loop converts algorithm indexes earned to the writable data
    for i in range(len(clusters)):
        for j in range(len(clusters[i])):
            output[i][j] = data[clusters[i][j]]

    # Writing data to kmeans table
    if algorithm == 'kmeans':
        for i in range(h):
            for j in range(w):
                # This line write to table needed columns from 0 index to end
                # index 0 for: device_id, 1 for ts, 2 for user_email, 3 for time_zone, 4 for location
                # 5 for latitude, 6 for longitude, 7 for tr_point, 8 for accessibility
                # We can decide which one to insert but if we changed this line we should change it in the
                # sql file related to this function that for this function is in the path:
                # ModelModule/query/insert_kmeans.sql
                # And surely u have to change the table according to the change u applied here
                cur.execute(insert_kmeans_qy, [output[i][j][0], output[i][j][1], output[i][j][5], \
                output[i][j][6], output[i][j][5], output[i][j][6], output[i][j][8]])
    if algorithm == 'dbscan':
        for i in range(h):
            for j in range(w):
                cur.execute(insert_dbscan_qy, [output[i][j][0], output[i][j][1], output[i][j][5], \
                output[i][j][6], output[i][j][5], output[i][j][6], output[i][j][8]])
    if algorithm == 'optics':
        for i in range(h):
            for j in range(w):
                cur.execute(insert_optics_qy, [output[i][j][0], output[i][j][1], output[i][j][5], \
                output[i][j][6], output[i][j][5], output[i][j][6], output[i][j][8]])

    cur.close()
    conn.commit()


# This function will insert the limit integer to the limit table
def insert_limit_data(limit):
    cur = conn.cursor()
    cur.execute(insert_limit_qy, [limit])
    cur.close()
    conn.commit()

# This function reads limit integer from limit table
def get_limit_data():
    cur = conn.cursor()
    cur.execute(get_limit_qy)
    res = cur.fetchall()
    cur.close()
    if not res:
        return 1
    else:
        return res[0][0]
