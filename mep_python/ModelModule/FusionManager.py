#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import os
import pandas as pd
import math
import shutil
import subprocess
import numpy as np

from ModelModule.CoordConverter import ENU2LLA
from datetime import datetime

minute, iter_num = 2, 0
mtf_len, frame_length_nmea = minute * 60 * 1000, minute * 60
target_file = ['MEP_MOTION_INFO.txt', 'MEP_NMEA_INFO.txt', 'PoseSE3(W).log']
string_list_motion = [',,,']  # the string to be removed from the output motion file
string_list_nmea = [',,,,,']  # the string to be removed from the output nmea file
mapping = range(1, 31)

slice_extremes_notify = '[INFO::FusionManager] Initial Timestamp: %s =====> Final Timestamp: %s'
frame_number_notify = '[INFO::FusionManager] nmea frame number: %s'
cur_pose_path_notify = '[INFO::FusionManager] Current path is: %s'
first_valid_fix_notify = '[INFO::FusionManager] First valid fix => x: %s, y: %s, z:%s'
broken_row_error = '[ERROR::FusionManager] Exists a broken row in %s ====> %s'
mep_fusion_error = '[ERROR::FusionManager] Error Occurred in mep-fusion algorithm at %s'


def _missing_handler(dfm_tmp):
    sensor_type = dfm_tmp.iloc[0, 1]
    col = 5
    if not (sensor_type.find('UNCALIBRATED') == -1):
        col = 8
    if dfm_tmp.iloc[:, 0:col].isnull().values.any():
        dfm_tmp.iloc[:, 0:col].fillna(0)
    return dfm_tmp.reset_index(drop=True)
    #return dfm_tmp


def _timestamp_spread(dfm_tmp):
    """
    MOTION_INFO.txt files result to have multiple sensor's measurements grouped on a single timestamp. This is an
    undesirable behavior produce by the android system which write a block of measurements stored in the buffer in a
    trance appending the current timestamp for all of them, even if they where not recorded in the same instant.
    This is an undesirable behavior which should be fixed.
    :param dfm_tmp: data frame motion tmp is the current data frame analyzed
    :return: the fixed data frame motion tmp
    """
    dup_el = dfm_tmp[dfm_tmp.duplicated(['1'])].drop_duplicates(['1'], keep='last')
    for ii in dup_el['1']:
        dfm_tmp_index = dfm_tmp[dfm_tmp['1'] == ii].index.tolist()
        # idx_dp is the index of the first element of the duplicated list ===> idx_pre is the index of the element
        # which precede the first element in the duplicated list
        idx_dp, idx_pre = dfm_tmp_index[0], max((dfm_tmp_index[0] - 1), 0)
        # ts_cur is the current timestamp analyzed ===> ts_prev is the first missing timestamp
        # n_dup is the number of duplicated elements
        ts_cur, ts_prev, n_dup = int(dfm_tmp.iloc[idx_dp, 0]), int(dfm_tmp.iloc[idx_pre, 0]) + 1, len(dfm_tmp_index)

        if (ts_cur - ts_prev) > 1:
            # Reconstructed timestamps
            delta = (ts_cur + 1 - ts_prev) / n_dup
            for n in range(0, len(dfm_tmp_index) - 1):
                rec_ts = int(np.floor(ts_prev + n * delta))
                dfm_tmp.ix[dfm_tmp_index[n], 0] = rec_ts
    return dfm_tmp


def _duplicate_handler(dfm_tmp):
    def _alter_element(n, idx):
        dfm_tmp.ix[idx[-1], n] = np.mean(dfm_tmp.iloc[idx[:], n])

    sensor_type = dfm_tmp.iloc[0, 1]
    col = 4
    if not (sensor_type.find('UNCALIBRATED') == -1):
        col = 7
    dup_el = dfm_tmp[dfm_tmp.duplicated(['1'])].drop_duplicates(['1'], keep='last')
    for ii in dup_el['1']:
        dfm_tmp_index = dfm_tmp[dfm_tmp['1'] == ii].index.tolist()
        map(_alter_element, range(2, col + 1), dfm_tmp_index)
    return dfm_tmp.drop_duplicates(['1'], keep='last').reset_index(drop=True)


def _cleaner1(file_name, stop_list):
    f = open(file_name, "r+")
    d = f.readlines()
    f.seek(0)
    for i in d:
        if i not in stop_list:
            f.write(i)
    f.truncate()
    f.close()


def _cleaner(file_name, stop_list):
    f = open(file_name, 'r')
    lst = []
    for line in f:
        for word in stop_list:
            if word in line:
                line = line.replace(word, '')
        lst.append(line)
    f.close()
    f = open(file_name, 'w')
    for line in lst:
        f.write(line)
    f.close()


def fusion_handler(cur_path):
    global target_file, minute, iter_num, frame_length_nmea, mtf_len
    global slice_extremes_notify, frame_number_notify, cur_pose_path_notify, first_valid_fix_notify
    global broken_row_error, mep_fusion_error
    global mapping

    msg = []

    map_motion = ['1', '2', '3', '4', '5', '6', '7', '8']
    # ========>  ['timestamp', 'sensor_type', 'x', 'y', 'z', '_x', '_y', '_z']

    map_nmea = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17',
                '18', '19', '20']
    # =======> ['type', 'fix', 'lat', 'long', 'quality', 'sat_num', 'hor_dil', 'altitude', 'height_geoid',
    #           'seconds', 'id_station', 'checksum']

    dfm_path, dfn_path = os.path.join(cur_path, target_file[0]), os.path.join(cur_path, target_file[1])

    # dfm and dfn represent respectively the data frame motion and the data frame nmea
    dfm, dfn = pd.read_csv(dfm_path, names=map_motion), pd.read_csv(dfn_path, names=map_nmea)

    """
    ----------------------------------- Data set Cleaning Step -----------------------------------------------------
    For each sensor type (TYPE_ACCELEROMETER, TYPE_GYROSCOPE, ...) clean the initial data set by filling the NULL
    values with zeros (may also be adopted a different policy according to data mining missing values theory).
    Moreover data which have been sampled with a too high frequency have to be sub-sampled in order for the
    algorithm from Bardaro to work.
    """

    tmp_path = os.path.join(cur_path, 'tmp')
    dfm_crt_path = os.path.join(tmp_path, 'MEP_MOTION_INFO_CORRECTED.txt')

    # Check if tmp folder exists or not
    if not os.path.isdir(tmp_path):
        os.mkdir(tmp_path)

    if not os.path.isfile(dfm_crt_path):

        gyrou_crt = dfm[dfm['2'] == 'TYPE_GYROSCOPE_UNCALIBRATED'].iloc[:, 0:8]
        gyrou_crt = _duplicate_handler(_timestamp_spread(_missing_handler(gyrou_crt)))

        magu_crt = dfm[dfm['2'] == 'TYPE_MAGNETIC_FIELD_UNCALIBRATED'].iloc[:, 0:8]
        magu_crt.append(_duplicate_handler(_timestamp_spread(_missing_handler(magu_crt))))

        acc_crt = dfm[dfm['2'] == 'TYPE_ACCELEROMETER'].iloc[:, 0:5]
        acc_crt.append(_duplicate_handler(_timestamp_spread(_missing_handler(acc_crt))))

        gyro_crt = dfm[dfm['2'] == 'TYPE_GYROSCOPE'].iloc[:, 0:5]
        gyro_crt.append(_duplicate_handler(_timestamp_spread(_missing_handler(gyro_crt))))

        mag_crt = dfm[dfm['2'] == 'TYPE_MAGNETIC_FIELD'].iloc[:, 0:5]
        mag_crt.append(_duplicate_handler(_timestamp_spread(_missing_handler(mag_crt))))

        # dfm_crt: data frame containing the MOTION_INFO.txt cleaned from the NA values
        # Join together the corrected cleaned data frames
        dfm_crt = pd.concat([magu_crt, gyrou_crt, acc_crt, gyro_crt, mag_crt])

        # join the cleaned sensor's block coming out from the cleaning process
        dfm_crt = dfm_crt.sort_values(by='1')

        # Remove duplicates based on {timestamp,sensor_type} key, keep only the last one
        dfm_crt = dfm_crt.drop_duplicates(['1', '2'], keep='last')

        dfm_crt.to_csv(dfm_crt_path, header=False, index=False)

    else:
        dfm_crt = pd.read_csv(dfm_crt_path, names=map_motion)

    # Compute initial and final timestamp to perform the separation of the MEP_MOTION_INFO file
    ts_start, ts_end = dfm_crt.iloc[0, 0], dfm_crt.iloc[-1, 0]

    msg.append(slice_extremes_notify % (ts_start, ts_end))

    """
    ----------------------------------- Block Subdivision ----------------------------------------------------------
    After the data have been cleaned they have to be separated into smaller portion of limited time, i.e.
    frame_length defined at the beginning of the file. In order efficiently compute the correction with the
    Bardaro's algorithm the frame_length should not be too high (in the order of 1/2 minutes). frame_length can be
    used as a tuning parameter by which smaller or bigger block can be considered.
    """

    # The only interesting data in the NMEA file are GPGGA one, they they are collected from the original NMEA file
    gpgga_df = dfn[dfn['1'] == '$GPGGA'].copy()
    f_num = math.ceil(gpgga_df.shape[0] / (60 * minute))
    msg.append(frame_number_notify % f_num)

    out_pose = []

    for ii in range(ts_start, ts_end, mtf_len):

        fold_n = os.path.join(tmp_path, 'fold%s' % ii)

        if not os.path.isdir(fold_n):
            os.mkdir(fold_n)

        msg.append(cur_pose_path_notify % fold_n)

        # MotSplPth contain the path to the now split MEP_MOTION_INFO.txt
        # NmeaSplPth contain the path to the now split MEP_NMEA_INFO.txt
        MotSplPth, NmeaSplPth = os.path.join(fold_n, target_file[0]), os.path.join(fold_n, target_file[1])

        # Extract the proper time range from the motion_corrected data frame
        dfm_crt[dfm_crt['1'].isin(range(ii, min(ii + mtf_len -1, ts_end)))].to_csv(MotSplPth, header=False, index=False)

        _cleaner(MotSplPth, string_list_motion)

        # Create the proper time range to extract the correct interval from the NMEA file
        timeFrt = '%Y-%m-%d %H:%M:%S'
        lo_th = datetime.utcfromtimestamp(ii / 1000).strftime(timeFrt)[-8:]
        up_th = datetime.utcfromtimestamp(min(ii + mtf_len - 1, ts_end) / 1000).strftime(timeFrt)[-8:]

        time_range = pd.date_range(lo_th, up_th, freq='1s').strftime('%H%M%S')

        # Write the extracted data into a new ad-hoc created NMEA file
        gpgga_df.loc[gpgga_df['2'].isin(time_range)].to_csv(NmeaSplPth, header=False, index=False)

        _cleaner(NmeaSplPth, string_list_nmea)

        """
        ----------------------------------- Bardaro Execution ------------------------------------------------------
        Once the data have been separated into smaller block the Bardaro's algorithm can effectively been executed.
        A system call is performed and the proper arguments passed to the function, i.e. cur_fold = the current
        folder and iter_num = the number of iteration that the algorithm have to perform to clean the data, this
        number must be between 100 and 150 in order to perform a better correction.
        When the algorithm has been executed the result is read from the standard error and parsed in order to get 7
        the first valid fix point in order to perform the conversion from enu to lla coordinate system.
        """
        FvFlog_path = os.path.join(fold_n, 'FvFlog.txt')
        Pose_path = os.path.join(fold_n, 'results', target_file[2])

        if os.path.isfile(Pose_path) and os.path.isfile(FvFlog_path):
            """
            If the FvFlog.txt and the PoseSE3(W) exist they are just loaded from the proper file.
            """
            pose_ii = pd.read_csv(Pose_path, names=mapping)
            fvf = np.loadtxt(FvFlog_path)
            datum_lla = pd.DataFrame(list(map(lambda i: ENU2LLA(i, fvf, 'dms'), np.array(pose_ii.ix[:, 23:25]))))
            timestamp = pd.DataFrame(list(map(lambda i: int(round(float(i) * 1000)), pose_ii.ix[:, 1])))
            out_pose.append(pd.concat([timestamp, datum_lla], axis=1))

        else:
            try:
                # System call to the mep-fusion algorithm
                cmd = ['./mep-roamfree/build/fusion', '%s' % fold_n, '%s' % iter_num]
                p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                out, err = p.communicate()

                err = str(err).split('\\n')

                for jj in err:
                    if not (jj.find('first valid fix') == -1):
                        xyz_ = jj.split(' ')
                        fvf = [float(xyz_[-3]), float(xyz_[-2]), float(xyz_[-1])]
                        msg.append(first_valid_fix_notify % fvf)
                        np.savetxt(FvFlog_path, fvf)

                pose_ii = pd.read_csv(Pose_path, names=mapping)
                datum_lla = pd.DataFrame(list(map(lambda i: ENU2LLA(i, fvf, 'dms'), pose_ii.ix[:, 23:25])))
                timestamp = pd.DataFrame(list(map(lambda i: int(round(float(i) * 1000)), pose_ii.ix[:, 1])))
                out_pose.append(pd.concat([timestamp, datum_lla], axis=1))

            except:
                msg.append(mep_fusion_error % cur_path)

    pd.DataFrame(pd.concat(out_pose)).to_csv(os.path.join(cur_path, 'TPoseSE(W)'), header=False, index=False)
    return msg
