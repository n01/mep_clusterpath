#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

from scipy import *
from math import pi
from numpy import cos, sin, arctan, arctan2

"""
CoordinateConverter operates all the conversion between the different coordinate systems.

-Earth Centred Earth Fixed (ECEF) coordinates
    The Earth-centred Earth-fixed (ECEF) or conventional terrestrial coordinate system rotates with the Earth and has
    its origin at the centre of the Earth. The X axis passes through the equator at the prime meridian. The Z axis
    passes through the north pole. The Y axis can be determined by the right-hand rule to be passing through the
    equator at 90 longitude.

-Local east, north, up (ENU) coordinates
    In many targeting and tracking applications the local East, North, Up (ENU) Cartesian coordinate system is far more
    intuitive and practical than ECEF or Geodetic coordinates. The local ENU coordinates are formed from a plane tangent
    to the Earth's surface fixed to a specific location and hence it is sometimes known as a "Local Tangent" or "local
    geodetic" plane. By convention the east axis is labeled x, the north y and the up z.
"""

F = 1.0 / 298.257223563                                                        # reciprocal flattening
A = 6378137.0                                                                  # earth semimajor axis in meters
E2 = F * (2 - F)                                                               # first eccentricity squared
EP2 = F * (2 - F) / ((1 - F) ** 2)                                             # second eccentricity squared
E = 8.1819190842622e-2
B = 6356752.314245179232792828844494830628254                                  # semi-minor axis
ep = 0.082094437949696195116048215728704722466497716759153


def LLA2ECEF(lla, in_ty='dms'):
    """
    LLA to ECEF conversion
    :param lla: the lla point expressed in degree
    :param in_ty: the input type of the data, it could be 'dms' to indicate degree-minute-second or 'dd' decimal degree
    :return: the point converted in ECEF coordinate system
    """
    global E2
    global A

    def _dms2dd(coord): return float(str(coord)[0:-9]) + float(str(coord)[-9:]) / 60.

    if in_ty == 'dms':
        lat_, long_, h = _dms2dd(lla[0]) / 180 * pi, _dms2dd(lla[1]) / 180 * pi, lla[2]
    elif in_ty == 'dd':
        lat_, long_, h = lla[0] / 180 * pi, lla[1] / 180 * pi, lla[2]  # to radians conversion
    elif in_ty != 'dms' and in_ty != 'dd':
        return "The required data type is not available!"

    chi = sqrt(1 - E2 * (sin(lat_)) ** 2)

    # ===========> Conversion lla2ecef #############################################################################

    return (A/chi + h) * cos(lat_) * cos(long_), (A/chi + h) * cos(lat_) * sin(long_), (A*(1 - E2)/chi + h) * sin(lat_)


def ECEF2LLAdms(ecef):
    X, Y, Z = ecef

    r2 = X ** 2 + Y ** 2
    r = sqrt(r2)
    E2_ = A ** 2 - B ** 2
    F_ = 54 * B ** 2 * Z ** 2
    G = r2 + (1 - E2) * Z ** 2 - E2 * E2_
    c = (E2 * E2 * F_ * r2) / (G * G * G)
    s = (1 + c + sqrt(c * c + 2 * c)) ** (1 / 3)
    P = F / (3 * (s + 1. / s + 1) ** 2. * G * G)
    Q = sqrt(1 + 2 * E2 * E2 * P)
    ro = -(E2 * P * r)/(1 + Q) + sqrt((A * A/2) * (1 + 1./Q) - ((1 - E2) * P * Z ** 2)/(Q * (1 + Q)) - P * r2/2)
    tmp = (r - E2 * ro) ** 2
    U = sqrt(tmp + Z ** 2)
    V = sqrt(tmp + (1 - E2) * Z ** 2)
    zo = (B ** 2 * Z) / (A * V)

    h_ = U * (1 - B ** 2. / (A * V))
    phi_ = arctan((Z + EP2 * zo) / r)
    lambda_ = arctan2(Y, X)

    return phi_ * 180 / pi, lambda_ * 180 / pi, h_


def LLA2ENU(lla, lla_ref, lla_type='dms', lla_ref_type='dms'):
    """

    :param lla: the lla point
    :param lla_ref: the lla reference point
    :param lla_type: the format type of the lla input data, 'dms' for degree-minute-second or 'dd' for decimal degree
    :param lla_ref_type: the format type of the lla_ref input data, 'dms' for degree-minute-second or 'dd' for decimal
                         degree
    :return: the point converted in ENU coordinate system
    """
    global E2
    global A

    if lla_type != 'dms' and lla_type != 'dd' or lla_ref_type != 'dms' and lla_ref_type != 'dd':
        return "The required data type is not available!"

    # ===========> LLA to ECEF conversion of the two points ############################################################
    X, Y, Z = LLA2ECEF(lla, lla_type)
    X_ref, Y_ref, Z_ref = LLA2ECEF(lla_ref, lla_ref_type)

    # ===========> Conversion ecef2enu #############################################################################

    e_ = -sin(lla_ref[1]) * (X - X_ref) + cos(lla_ref[1]) * (Y - Y_ref)
    n_ = -sin(lla_ref[0]) * cos(lla_ref[1]) * (X - X_ref) - sin(lla_ref[0]) * sin(lla_ref[1]) * (Y - Y_ref) \
        + cos(lla_ref[0]) * (Z - Z_ref)
    u_ = cos(lla_ref[0]) * cos(lla_ref[1]) * (X - X_ref) + cos(lla_ref[0]) * sin(lla_ref[1]) * (Y - Y_ref) \
        + sin(lla_ref[0]) * (Z - Z_ref)

    return e_, n_, u_


def ENU2LLA(enu, lla_ref, lla_ref_type='dms'):
    """

    :param enu: the enu point
    :param lla_ref: the lla reference point
    :param lla_ref_type: the type of the lla_ref, could be 'dms' for degree, minutes, seconds or 'dd' for decimal degree
    :return: the point converted in LLA coordinate system expressed in decimal degree
    """
    global A
    global E2
    global B
    global EP2
    global E

    if lla_ref_type != 'dms' and lla_ref_type != 'dd':
        return "The required data type is not available!"
    # ===========> Conversion lla2ecef of the reference point ##########################################################

    X_ref, Y_ref, Z_ref = LLA2ECEF(lla_ref, lla_ref_type)

    # ===========> Conversion enu2ecef #################################################################################

    X = -sin(lla_ref[0]) * E - cos(lla_ref[1]) * sin(lla_ref[0]) * enu[1] + cos(lla_ref[1]) * \
        cos(lla_ref[0]) * enu[2] + X_ref
    Y = cos(lla_ref[1]) * enu[0] - sin(lla_ref[1]) * sin(lla_ref[0]) * enu[1] + cos(lla_ref[0]) * \
        sin(lla_ref[1]) * enu[2] + Y_ref
    Z = cos(lla_ref[0]) * enu[1] + sin(lla_ref[0]) * enu[2] + Z_ref

    return ECEF2LLAdms([X, Y, Z])

