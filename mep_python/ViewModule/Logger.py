#############################
# Autore:   Andrea Peccini
# Mail: pecciniandrea@gmail.com
# Data:     24/04/2017
###############################

import os
import logging
import configparser
import datetime as dt

config = configparser.ConfigParser()
config.read('config.txt')
testing = config['Devel_Info'].getboolean('testing')


class InfoLogger:

    def __init__(self):
        """
        Constructor of the InfoLogger class
        """
        log_date = dt.datetime.now().strftime("%Y%m%d-%H%M%S")
        if not os.path.exists('log'):
            os.mkdir('log')
        logging.basicConfig(filename='log/mep%s.log' % log_date, level=logging.INFO)

    @staticmethod
    def write2log(input_info):
        """
        write to the log file each event encountered in the program execution.
        :param input_info: the input information to write in the log file
        :return: None
        """
        global testing

        if isinstance(input_info, list):
            for item in input_info:
                logging.info(str(dt.datetime.now()) + '	' + item)
                if testing:
                    print(str(dt.datetime.now()) + '	' + item)
        else:
            logging.info(str(dt.datetime.now()) + '	' + str(input_info))
            if testing:
                print(str(dt.datetime.now()) + '	' + input_info)
