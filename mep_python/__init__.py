# ------------------------------------- MEP_py_script ------------------------------------------------------------------
from ControllerModule.Controller import Controller

if __name__ == '__main__':
    # Define an instance of controller
    controller = Controller()
    # Start the controller main loop
    controller.main_loop()
