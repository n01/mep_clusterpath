#!/bin/bash


# sudo -u postgres psql mep_neural_test

# Requirements for the dica script to work
add-apt-repository ppa:ubuntugis/ppa && sudo apt-get update
apt-get install gdal-bin

# Vars
#DB=mep_neural_test
DB=mep_db

# ======================= Table ========================================================================

GPSPP="﻿CREATE TABLE public.type_gps_position_point
(
    device_id VARCHAR(16), 		-- device_id of the user
    ts BIGINT, 				-- serialized timestamp
    user_email CHAR(50), 		-- the user email
    time_zone TIMESTAMP, 		-- timestamp
    location VARCHAR(30), 		-- location of the acquisition
    latitude DOUBLE PRECISION, 		-- latitude of the point
    longitude DOUBLE PRECISION, 	-- longitude of the point
    tr_point GEOMETRY, 			-- trace points
    accessibility_level INT, 		-- accessibility level of the current point
    PRIMARY KEY ( device_id , ts ) 	-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "$GPSPP"

GPSPPqry= "\dS+ type_gps_position_point;"

psql -d $DB -c "$GPSPPqry"

# ======================================================================================================
#
#                                Table "public.type_gps_position_point"
#       Column        |            Type             | Modifiers | Storage  | Stats target | Description
#---------------------+-----------------------------+-----------+----------+--------------+-------------
# device_id           | character varying(16)       | not null  | extended |              |
# ts                  | bigint                      | not null  | plain    |              |
# user_email          | character(50)               |           | extended |              |
# time_zone           | timestamp without time zone |           | plain    |              |
# location            | character varying(30)       |           | extended |              |
# latitude            | double precision            |           | plain    |              |
# longitude           | double precision            |           | plain    |              |
# tr_point            | geometry                    |           | main     |              |
# accessibility_level | integer                     |           | plain    |              |
#Indexes:
#    "type_gps_position_point_pkey" PRIMARY KEY, btree (device_id, ts)
#Triggers:
#    check_duplicate BEFORE INSERT ON type_gps_position_point FOR EACH ROW EXECUTE PROCEDURE duplicate_point_trigger()

# ======================= Schema =======================================================================

BUISCHEMA="CREATE SCHEMA building
  AUTHORIZATION postgres;"

psql -d $DB -c "$BUISCHEMA"

BUIMP='CREATE TABLE building.multipolygons
(
  ogc_fid serial NOT NULL,
  osm_id character varying,
  osm_way_id character varying,
  name character varying,
  type character varying,
  aeroway character varying,
  amenity character varying,
  admin_level character varying,
  barrier character varying,
  boundary character varying,
  building character varying,
  craft character varying,
  geological character varying,
  historic character varying,
  land_area character varying,
  landuse character varying,
  leisure character varying,
  man_made character varying,
  military character varying,
  "natural" character varying,
  office character varying,
  place character varying,
  shop character varying,
  sport character varying,
  tourism character varying,
  other_tags character varying,
  wkb_geometry geometry(MultiPolygon,4326),
  CONSTRAINT multipolygons_pkey PRIMARY KEY (ogc_fid)
);'

psql -d $DB -c "$BUIMP"

# ======================= Schema =======================================================================

HIGHSCHEMA="CREATE SCHEMA highway
  AUTHORIZATION postgres;"

psql -d $DB -c "$HIGHSCHEMA"

HIGHLN="CREATE TABLE highway.lines
(
  ogc_fid serial NOT NULL,
  osm_id character varying,
  name character varying,
  highway character varying,
  waterway character varying,
  aerialway character varying,
  barrier character varying,
  man_made character varying,
  z_order integer,
  other_tags character varying,
  wkb_geometry geometry(LineString,4326),
  CONSTRAINT lines_pkey PRIMARY KEY (ogc_fid)
);"

psql -d $DB -c "$HIGHLN"

# ======================= Table ========================================================================

GPSDtest="create table public.pose_pt
(
    device_id varchar(16), 		-- device_id of the user
    ts bigint, 				-- serialized timestamp
    user_email varchar(50), 		-- the user email
    time_zone timestamp, 		-- timestamp
    location varchar(30), 		-- location of the acquisition
    latitude double precision, 		-- latitude of the point
    longitude double precision, 	-- longitude of the point
    coord geometry, 			-- corrected points
    accessibility_level int, 		-- accessibility level of the current point
    primary key (device_id, ts)  	-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "$GPSDtest"

GPSDqry= "\dS+ gpsdata_test;"

psql -d $DB -c "$GPSDqry"

# ======================================================================================================
#
#                                      Table "public.gpsdata_test"
#       Column        |            Type             | Modifiers | Storage  | Stats target | Description
#---------------------+-----------------------------+-----------+----------+--------------+-------------
# id                  | integer                     | not null  | plain    |              |
# device_id           | character varying(16)       |           | extended |              |
# ts                  | bigint                      |           | plain    |              |
# user_email          | character(50)               |           | extended |              |
# time_zone           | timestamp without time zone |           | plain    |              |
# location            | character varying(30)       |           | extended |              |
# latitude            | double precision            |           | plain    |              |
# longitude           | double precision            |           | plain    |              |
# coord               | geometry                    |           | main     |              |
# accessibility_level | integer                     |           | plain    |              |
# time                | time without time zone      |           | plain    |              |
# Indexes:
#    "gpsdata_test_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

GPSDtest_corr="create table public.pose_pt_corr
(
    device_id varchar(16), 		-- device_id of the user
    ts bigint, 				-- serialized timestamp
    user_email varchar(50), 		-- the user email
    time_zone timestamp, 		-- timestamp
    location varchar(30), 		-- location of the acquisition
    latitude double precision, 		-- latitude of the point
    longitude double precision, 	-- longitude of the point
    coord geometry, 			-- corrected points
    accessibility_level int, 		-- accessibility level of the current point
    primary key (device_id, ts)  	-- primary key is the pair device_id and serialized timestamp
);"

psql -d $DB -c "$GPSDtest_corr"

GPSDqryc= "\dS+ gpsdata_test_corr;"

psql -d $DB -c "$GPSDqryc"

# ======================================================================================================
#
#                                   Table "public.gpsdata_test_corr"
#       Column        |            Type             | Modifiers | Storage  | Stats target | Description
#---------------------+-----------------------------+-----------+----------+--------------+-------------
# id                  | integer                     |           | plain    |              |
# device_id           | character varying(16)       |           | extended |              |
# ts                  | bigint                      |           | plain    |              |
# user_email          | character(50)               |           | extended |              |
# time_zone           | timestamp without time zone |           | plain    |              |
# location            | character varying(30)       |           | extended |              |
# latitude            | double precision            |           | plain    |              |
# longitude           | double precision            |           | plain    |              |
# coord               | geometry                    |           | main     |              |
# accessibility_level | integer                     |           | plain    |              |
# time                | time without time zone      |           | plain    |              |


# ======================= Table ========================================================================

BUPER="﻿create table public.building_perimeter
(	id serial not null,		-- The id of the row
	building_id bigint not null, 	-- The id of the building
	name character varying,		-- The name of the building
	perimeter geometry,		-- The shape of the building
	primary key (id)		-- The primary key is fixed to be the id(this may be changed in a better way)
);"

psql -d $DB -c "$BUPER"tests data sets creation removed

BUPERqry= "\dS+ build_perimiter;"

psql -d $DB -c "$BUPERqry"

# ======================================================================================================
#
#                         Table "public.build_perimiter"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# line   | geometry          |           | main     |              |
#Indexes:
#    "build_perimiter_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

BUSEG="create table public.building_segment
(
	id serial not null,		-- The id of the row
	building_id bigint not null,	-- The id of the building
	name character varying,		-- The name of the building
	segment geometry,		-- The segment extracted from the building
	primary key(id)			-- The primary key of the table
);"

psql -d $DB -c "$BUSEG"

BUSEGqry= "\dS+ build_segment;"

psql -d $DB -c "$BUSEGqry"

# ======================================================================================================
#
#                          Table "public.build_segment"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# seg    | geometry          |           | main     |              |
# idpoly | integer           |           | plain    |              |
#Indexes:
#    "build_segment_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

BUILDING="create table public.building_area
(	id serial not null,		-- The id of the row
	building_id bigint not null,	-- The id of the building
	name character varying,		-- The name
	area geometry,			-- The shape of the building
	primary key (id)		-- The primary key is fixed to be the id(this may be changed in a better way)
);"

psql -d $DB -c "$BUILDING"

BUILDINGqry= "\dS+ building;"

psql -d $DB -c "$BUILDINGqry"

# ======================================================================================================
#
#                            Table "public.building"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# polyg  | geometry          |           | main     |              |
#Indexes:
#    "building_pkey" PRIMARY KEY, btree (id)


# ======================= Table ========================================================================

HIGHSEG="create table public.highway_segment
(
	id serial not null,
	highway_id bigint not null,
	osm_id character varying,
	name character varying,
	segment geometry,
	primary key(id)
);"

psql -d $DB -c "$HIGHSEG"

HIGHSEGqry= "\dS+ highway_segment;"

psql -d $DB -c "$HIGHSEGqry"

# ======================================================================================================
#
#                         Table "public.highway_segment"
# Column |       Type        | Modifiers | Storage  | Stats target | Description
#--------+-------------------+-----------+----------+--------------+-------------
# id     | integer           | not null  | plain    |              |
# name   | character varying |           | extended |              |
# seg    | geometry          |           | main     |              |
#Indexes:
#    "highway_segment_pkey" PRIMARY KEY, btree (id)
