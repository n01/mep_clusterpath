#!/bin/bash


## ------------------------------- Install python 3.5 ----------------------------------------------
# apt-get install -y libssl-dev openssl
# wget https://www.python.org/ftp/python/3.5.0/Python-3.5.0.tgz
# tar xzvf Python-3.5.0.tgz
# cd Python-3.5.0
# ./configure
# make && make install

# ./installPython_ubuntuAMD64.sh
# ./postgres_installer.sh

## ------------------------------- ROAMFREE --------------------------------------------------------
# Dependencies
sudo apt-get install -y libsuitesparse-dev libeigen3-dev libboost-all-dev
sudo apt-get install -y libopencv-dev python-opencv
sudo apt-get install -y gsl-bin libgsl0-dev
# add-apt-repository -y ppa:george-edison55/cmake-3.x
# apt-get update
sudo apt-get install -y cmake
echo "Dependencies installation completed.........................................................."

# Download ROAMFREE
# git clone https://andreapecs@bitbucket.org/airlab-polimi/roamfree_private.git
git clone https://github.com/AIRLab-POLIMI/ROAMFREE.git
cd ROAMFREE/
git fetch && git checkout dev
mkdir build && cd build
sudo cmake ../roamfree
sudo make && make install
cd /usr/local/include
cp -r roamfree/* /usr/local/include
# Change the cmake file of the mep-roamfree folder should be better

## ------------------------------- MEP-fusion ------------------------------------------------------
# Back to the mep-roamfree folder
cd mep-roamfree
mkdir build && cd build
sudo cmake ../
sudo make
echo "Mep-roamfree built completed................................................................."

## ------------------------------- Python libraries ------------------------------------------------
# Dependencies && library
sudo apt-get install -y libpq-dev
export PATH=/usr/lib/postgresql/X.Y/bin/:$PATH
sudo python3.5 -m pip install psycopg2
sudo python3.5 -m pip install pandas
sudo python3.5 -m pip install numpy
sudo python3.5 -m pip install scipy
sudo python3.5 -m pip install threading
sudo python3.5 -m pip install queue
sudo python3.5 -m pip install watchdog
sudo python3.5 -m pip install logging
echo "Python3.5 requirements are now satisfied ----------------------------------------------------"
